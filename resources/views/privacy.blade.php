@extends('layouts.app')
@section('content')

<div class="privacy">
    <div class="container py-5">
        <div class="card mb-4">
            <div class="card-body">
                <h2>Privacy  Policy</h2>
                <p>Privacy  Policy of&nbsp;<strong>ERA SOS</strong> <br>
                    This Application collects some Personal Data from its Users.
                </p>
                <hr>
                <h3><strong>Data Controller and Owner</strong><br>
                </h3>
                <p>Home Defense Technology Corporation <br>
                    Unit 502-B, 5th Floor Catleya Condo,<br>
                    235 Salcedo St, Legaspi Village, <br>
                    San Lorenzo Makati City, Philippines<br>
                    <strong>Owner contact email:</strong>&nbsp;<a href="mailto:hdtcph@gmail.com">hdtcph@gmail.com</a>
                </p>
                <hr>
                <h3><strong>Types of Data collected</strong></h3>
                <p>Among the types of Personal Data that this Application collects,  by itself or through third parties, there are: Approximate location permission  (non-continuous), </p>
                <p>Precise location permission (non-continuous), Phone  permission, SMS permission and Storage permission.<br>
                </p>
                <p>Other Personal Data collected may be described in other sections  of this privacy policy or by dedicated explanation text contextually with the  Data collection.<br>
                </p>
                <p>The Personal Data may be freely provided by the User, or collected  automatically when using this Application.<br>
                </p>
                <p>Any use of Cookies - or of other tracking tools - by this Application or by the  owners of third party services used by this Application, unless stated  otherwise, serves to identify Users and remember their preferences, for the  sole purpose of providing the service required by the User.<br>
                </p>
                <p>Failure to provide certain Personal Data may make it impossible for this Application  to provide its services.<br>
                </p>
                <p>Users are responsible for any Personal Data of third parties  obtained, published or shared through this Application and confirm that they  have the third party's consent to provide the Data to the Owner.</p>
                <hr>
                <h3><strong>Mode and place of processing the  Data</strong><br>
                </h3>
                <p><strong>Methods of processing</strong></p>
                <p>The Data Controller processes the Data of Users in a proper  manner and shall take appropriate security measures to prevent unauthorized  access, disclosure, modification, or unauthorized destruction of the Data.<br>
                </p>
                <p>The Data processing is carried out using computers and/or IT enabled tools,  following organizational procedures and modes strictly related to the purposes  indicated. In addition to the Data Controller, in some cases, the Data may be  accessible to certain types of persons in charge, involved with the operation  of the site (administration, sales, marketing, legal, system administration) or  external parties (such as third party technical service providers, mail  carriers, hosting providers, IT companies, communications agencies) appointed,  if necessary, as Data Processors by the Owner. The updated list of these  parties may be requested from the Data Controller at any time.</p>
                <p><strong>Place</strong></p>
                <p>The Data is processed at the Data Controller's operating offices  and in any other places where the parties involved with the processing are  located. For further information, please contact the Data Controller.</p>
                <p><strong>Retention time</strong></p>
                <p>The Data is kept for the time necessary to provide the service  requested by the User, or stated by the purposes outlined in this document, and  the User can always request that the Data Controller suspend or remove the  data.</p>
                <hr>
                <h3><strong>The use of the collected Data</strong></h3>
                <p>The Data concerning the User is collected to allow the Owner to  provide its services, as well as for the following purposes: Device permissions  for Personal Data access.</p>
                <p>The Personal Data used for each purpose is outlined in the  specific sections of this document.</p>
                <hr>
                <h3><strong>Device permissions for Personal  Data access</strong></h3>
                <p>This Application requests certain permissions from Users that  allow it to access the User's device Data as described below.<br>
                </p>
                <p>By default, these permissions must be granted by the User before  the respective information can be accessed. Once the permission has been given,  it can be revoked by the User at any time. In order to revoke these  permissions, Users may refer to the device settings or contact the Owner for  support at the contact details provided in the present document.</p>
                <p>The exact procedure for controlling app permissions may be dependent on the  User's device and software.<br>
                </p>
                <p>Please note that the revoking of such permissions might impact  the proper functioning of this Application.<br>
                </p>
                <p>If User grants any of the permissions listed below, the  respective Personal Data may be processed (i.e accessed to, modified or  removed) by this Application.</p>
                <p><strong>Approximate location permission  (non-continuous)</strong></p>
                <p>Used for accessing the User's approximate device location. This  Application may collect, use, and share User location Data in order to provide  location-based services.<br>
                </p>
                <p>The geographic location of the User is determined in a manner that isn't  continuous. This means that it is impossible for this Application to derive the  approximate position of the User on a continuous basis.</p>
                <p><strong>Phone permission</strong></p>
                <p>Used for accessing a host of typical features associated with  telephony. This enables, for instance, read-only access to the "phone state",  which means it enables access to the phone number of the device, current mobile  network information, or the status of any ongoing calls.</p>
                <p><strong>Precise location permission  (non-continuous)</strong></p>
                <p>Used for accessing the User's precise device location. This  Application may collect, use, and share User location Data in order to provide  location-based services.<br>
                </p>
                <p>The geographic location of the User is determined in a manner that isn't  continuous. This means that it is impossible for this Application to derive the  exact position of the User on a continuous basis.</p>
                <p><strong>SMS permission</strong></p>
                <p>Used for accessing features related to the User's messaging  including the sending, receiving and reading of SMS.</p>
                <p><strong>Storage permission</strong></p>
                <p>Used for accessing shared external storage, including the  reading and adding of any items.</p>
                <hr>
                <h3><strong>Detailed information on the  processing of Personal Data</strong></h3>
                <p>Personal Data is collected for the following purposes and using  the following services:</p>
                <p><strong>Device  permissions for Personal Data access</strong></p>
                <ul>
                    <li>This Application requests certain permissions from Users that  allow it to access the User's device Data as described below.</li>
                    <li><em><strong>Device permissions for  Personal Data access (This Application)</strong></em></li>
                    <li>This Application requests certain permissions from Users that  allow it to access the User's device Data as summarized here and described  within this document.</li>
                    <li>Personal Data collected: Approximate location permission  (non-continuous), Phone permission, Precise location permission  (non-continuous), SMS permission and Storage permission.</li>
                </ul>
                <hr>
                <h3><strong>Additional information about Data  collection and processing</strong></h3>
                <p><strong>Legal action</strong></p>
                <p>The User's Personal Data may be used for legal purposes by the  Data Controller, in Court or in the stages leading to possible legal action  arising from improper use of this Application or the related services.<br>
                </p>
                <p>The User declares to be aware that the Data Controller may be required to  reveal personal data upon request of public authorities.</p>
                <p><strong>Additional information about  User's Personal Data</strong></p>
                <p>In addition to the information contained in this privacy policy,  this Application may provide the User with additional and contextual  information concerning particular services or the collection and processing of  Personal Data upon request.</p>
                <p><strong>System logs and maintenance</strong></p>
                <p>For operation and maintenance purposes, this Application and any  third party services may collect files that record interaction with this  Application (System logs) or use for this purpose other Personal Data (such as  IP Address).</p>
                <p><strong>Information not contained in this  policy</strong></p>
                <p>More details concerning the collection or processing of Personal  Data may be requested from the Data Controller at any time. Please see the  contact information at the beginning of this document.</p>
                <p><strong>The rights of Users</strong><br>
                    Users have the right, at any time, to know whether their  Personal Data has been stored and can consult the Data Controller to learn  about their contents and origin, to verify their accuracy or to ask for them to  be supplemented, cancelled, updated or corrected, or for their transformation  into anonymous format or to block any data held in violation of the law, as  well as to oppose their treatment for any and all legitimate reasons. Requests  should be sent to the Data Controller at the contact information set out above.
                </p>
                <p>This Application does not support "Do Not Track" requests.<br>
                </p>
                <p>To determine whether any of the third party services it uses honor the "Do Not  Track" requests, please read their privacy policies.</p>
                <p><strong>Changes to this privacy policy</strong></p>
                <p>The Data Controller reserves the right to make changes to this  privacy policy at any time by giving notice to its Users on this page. It is  strongly recommended to check this page often, referring to the date of the  last modification listed at the bottom. If a User objects to any of the changes  to the Policy, the User must cease using this Application and can request that  the Data Controller remove the Personal Data. Unless stated otherwise, the  then-current privacy policy applies to all Personal Data the Data Controller  has about Users.</p>
                <p><strong>Information about this privacy  policy</strong></p>
                <p>The Data Controller is responsible for this privacy policy.</p>
                <hr>
                <h3><strong>Definitions and  legal references</strong></h3>
                <p><strong>Personal  Data (or Data)</strong></p>
                <p>Any information  regarding a natural person, a legal person, an institution or an association,  which is, or can be, identified, even indirectly, by reference to any other  information, including a personal identification number.</p>
                <p><strong>Usage Data</strong></p>
                <p>Information  collected automatically from this Application (or third party services employed  in this Application), which can include: the IP addresses or domain names of  the computers utilized by the Users who use this Application, the URI addresses  (Uniform Resource Identifier), the time of the request, the method utilized to  submit the request to the server, the size of the file received in response,  the numerical code indicating the status of the server's answer (successful  outcome, error, etc.), the country of origin, the features of the browser and  the operating system utilized by the User, the various time details per visit  (e.g., the time spent on each page within the Application) and the details  about the path followed within the Application with special reference to the sequence  of pages visited, and other parameters about the device operating system and/or  the User's IT environment.</p>
                <p><strong>User</strong></p>
                <p>The  individual using this Application, which must coincide with or be authorized by  the Data Subject, to whom the Personal Data refers.</p>
                <p><strong>Data Subject</strong></p>
                <p>The  legal or natural person to whom the Personal Data refers.</p>
                <p><strong>Data  Processor (or Data Supervisor)</strong></p>
                <p>The  natural person, legal person, public administration or any other body,  association or organization authorized by the Data Controller to process the  Personal Data in compliance with this privacy policy.</p>
                <p><strong>Data  Controller (or Owner)</strong></p>
                <p>The  natural person, legal person, public administration or any other body,  association or organization with the right, also jointly with another Data  Controller, to make decisions regarding the purposes, and the methods of  processing of Personal Data and the means used, including the security measures  concerning the operation and use of this Application. The Data Controller,  unless otherwise specified, is the Owner of this Application.</p>
                <p><strong>This  Application</strong></p>
                <p>The  hardware or software tool by which the Personal Data of the User is collected.</p>
                </div>
                </div>
                <div class="card mb-4">
                <div class="card-body">
                <h2>DATA PRIVACY ACT POLICY</h2>
                <p>Version 0 | August 14, 2020</p>
                <hr>
                <h3><strong>PRIVACY POLICY</strong><br>
                </h3>
                <p>
                    This Policy defines the commitment of <strong>Home Defense Technology (HDT)</strong> to collect and process personal
                    data in compliance with Republic Act No. 10173 or the Data Privacy Act of 2012 (DPA), its Implementing
                    Rules and Regulations, and other relevant policies.
                </p>
                <p>
                    It aims to inform clients, employees, partners and stakeholders of HDT’s data protection and security
                    measures, and to guide them in the exercise of their rights under the DPA and other relevant regulations
                    and policies.
                </p>
                <hr>
                <h3><strong>PROCESSING OF PERSONAL DATA</strong></h3>
                <ol style="list-style-type: upper-alpha;">
                    <li>
                        <strong>Collection</strong>
                        <p>HDT collects the following various types of information;</p>
                        <ol style="list-style-type:lower-roman;">
                            <li>
                                <strong>From Clients.</strong> This includes information used to identify and contact the client such as name,
                                job title, company name, company address, email address and phone number. 
                            </li>
                            <li>
                                <strong>From Job Applicants.</strong> This includes information used to identify and contact the applicant such
                                as name, home address, email address and phone number. 
                            </li>
                            <li>
                                <strong>From Business Parties and Third Parties.</strong> This includes information used to identify and
                                contact our business partner such as name, job title, company name, company address, email
                                address and phone number. 
                            </li>
                            <li>
                                <strong>From Cookies and Similar Technologies.</strong> This includes technical data collected from
                                analytics, cookies, tracking and similar technologies such as device used, IP address, links
                                visited and other similar information. 
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Use</strong>
                        <p>HDT collects and maintain personal data for the purposes of;</p>
                        <ul>
                            <li>
                                To perform our obligations per service agreement. We collect personal data to verify your
                                identity to ensure accurate delivery of our products and services. Such data is also used for
                                billing and collection of fees for the product and services provided.
                            </li>
                            <li>
                                To provide business operations support. We analyze your usage of our facilities to provide
                                operational support to your account, offer customer service activities, and address your
                                requests and concerns. We may also process your personal contact details in our database
                                to effectively communicate with you for proper assistance.
                            </li>
                            <li>
                                To continuously improve our products and services. We collect, use, and process your use of
                                our products and services so that we can better understand and improve them for your
                                benefit. It gives us better insight about the kinds of offers that would be relevant to your needs.
                            </li>
                            <li>
                                To secure our business operations. We may process your personal data to conduct security
                                operations check to ensure fair use of our products and services, for business continuity and
                                audits purposes.
                            </li>
                            <li>
                                To conduct online marketing. We use your personal data to effectively communicate and
                                provide offers and promotions.
                            </li>
                            <li>
                                To comply with legal requirements. We use and process personal data to address legal issues
                                such as complying to our obligations to retain business records for a minimum retention
                                periods, complying with laws, regulations and other legal process, violation of our agreements
                                and protecting HDT’s and your rights, property, welfare, health and safety.
                            </li>
                        </ul>
                    </li>
                    <li>
                        <strong>Storage, Retention and Destruction</strong>
                        <p>
                            HDT will ensure that personal data under its custody are protected against any accidental or unlawful
                            destruction, alteration and disclosure as well as against any other unlawful processing. HDT will
                            implement appropriate security measures in storing collected personal information, depending on the
                            nature of the information. All information gathered shall not be retained for a period longer than one
                            (1) year. After one (1) year, all hard and soft copies of personal information shall be disposed and
                            destroyed, through secured means.
                        </p>
                    </li>
                    <li>
                        <strong>Access</strong>
                        <p>
                            HDT values the sensitivity and confidentiality of the personal data under our custody, thus we ensure
                            that HDT maintains reasonable safeguards to protect your personal data. Only you and authorized
                            representative from HDT shall be allowed to access such data from our database for any purpose,
                            except for those contrary to law, public policy, public order or morals.
                        </p>
                    </li>
                    <li>
                        <strong>Disclosure and Sharing</strong>
                        <p>
                            All HDT employees and personnel shall maintain the confidentiality and secrecy of all personal data
                            that come to their knowledge and possession, even after resignation, termination of contract, or other
                            contractual relations. Personal data under the custody of the company shall be disclosed only pursuant
                            to a lawful purpose, and to authorized recipients of such data.
                        </p>
                    </li>
                </ol>
                <hr>
                <h3><strong>SECURITY MEASURES</strong><br></h3>
                <p>
                    HDT shall appoint a Data Protection Officer (DPO) who will be responsible in ensuring the organization’s
                    compliance with applicable laws and regulations for the protection of data privacy and security. The DPO
                    is responsible for the supervision and enforcement of this Policy.
                </p>
                <p>
                    In accordance to the enforcement of our Privacy Policy within the organization, HDT has implemented
                    technical, organizational, and physical security measures that are designed to protect personal data from
                    unauthorized or fraudulent access, alteration, disclosure, misuse, and other unlawful activities. These are
                    also designed to protect personal data from other natural and human dangers.
                </p>
                <p>HDT put in effect the following safeguards on personal data;</p>
                <ol style="list-style-type: lower-roman;">
                    <li>Protect in a secured server</li>
                    <li>Maintain as long as necessary to (a) provide the products and services, (b) for legal business
                        purposes, (c) to comply with pertinent laws, and (d) for special cases that will require the exercise
                        or defense of legal claims and for a defined retention period after termination of service</li>
                    <li>Restrict access to personal data to qualified and authorized personnel who are trained to handle
                        the data with strict confidentiality;</li>
                    <li>Regular audits and testing of infrastructure’s security protocols to ensure data is always protected;</li>
                    <li>Notify concerned individual and the competent data protection authority, when sensitive personal
                        data that may, under the circumstances, be used to enable identity fraud are reasonably believed
                        to have been acquired by an unauthorized person;</li>
                    <li>Allow update of information securely to keep our records accurate.</li>
                </ol>
                <hr>
                <h3><strong>RIGHT OF DATA SUBJECT</strong></h3>
                <p>As identified under DPA, data subjects have the following rights in connection with the processing of
                personal data</p>
                <ol style="list-style-type: lower-alpha;">
                    <li><strong>Right to object.</strong>Data subject has the right to refuse to the processing of personal data including
                        processing for direct marketing, automated processing or profiling.</li>
                    <li><strong>Right to access.</strong> Upon request, you may be given access to your personal data that has been
                        collected and processed.</li>
                    <li><strong>Right to rectification.</strong> Data subject has the right to dispute any inaccuracy or error in his/her
                        personal data, and the organization shall correct it immediately and accordingly, unless the request
                        is unreasonable.</li>
                    <li><strong>Right to erasure.</strong>Data subject has the right to request the erasure of the personal date, such as
                        in cases where the personal data is no longer necessary to achieve legitimate purpose of its use ir
                        processing</li>
                    <li><strong>Right to restrictions.</strong>This right entitles data subject to request to only process data in limited
                        circumstances, including consent given.</li>
                    <li><strong>Right to data portability.</strong>Data subject has the right to receive a copy of personal data that have
                        been provided to us, as well as, transmit a copy of such data to another company on data subject’s
                        behalf.</li>
                </ol>
                <hr>
                <h3><strong>BREACH AND SECURITY INCIDENTS</strong></h3>
                <p>HDT shall comply with the relevant provisions of rules and circulars on handling personal data security
                breaches, including notification to concerned individual or to the National Privacy Commission, where an
                unauthorized acquisition of sensitive personal information or information that may be used to enable identity
                fraud has been acquired by an unauthorized person, and is likely to give rise to a real risk of serious harm
                to the affected data subject. However, it should be noted that under applicable law, not all personal data
                breaches are notifiable.</p>
                <hr>
                <h3><strong>INQUIRIES AND COMPLAINTS</strong></h3>
                <p>If you have any questions about this Policy, specific privacy statement or our use of personal data, please
                contact our DPO at privacy.hdt@gmail.com. HDT shall ensure that all requests, inquiries and concerns are
                attended within thirty (30) days upon receipt.</p>
                <hr>
                <h3><strong>CHANGES TO PRIVACY POLICY</strong></h3>
                <p>Any changes to this Privacy Policy will be posted on this site, along with the information on any material
                changes. HDT reserves the right to update or modify this Privacy Policy and any specific privacy statement
                at any time without prior notice. Any modifications will apply only to personal data we collect after posting.</p>
                <p>This Privacy Statement is effective on August 14, 2020.</p>
            </div>
        </div>
    </div>
</div>

@endsection
