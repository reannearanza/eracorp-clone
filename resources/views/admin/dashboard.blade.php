@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Registrations</div>
                                <div class="card-body">
                                    <div id="search" class="py-4">
                                        <form action="/admin/registration/search" method="GET">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td style="width:50%">
                                                    <input name="search" class="form-control" type="text" placeholder="Search" aria-label="Search">
                                                    </td>
                                                    <td style="width:30%">
                                                        <select  name="by" id="" class="form-control">
                                                            <option value="email" selected>Email</option>
                                                            <option value="firstname">First name</option>
                                                            <option value="lastname">Last name</option>
                                                        </select>
                                                    </td>
                                                    <td style="width:20%">
                                                        <button class="btn btn-info btn-block">
                                                        <i class="fab fa-searchengin"></i>
                                                            Search
                                                        </button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>

                                    <table class="table table-responsive-sm table-hover table-outline mb-0">
                                        <thead class="thead-light">
                                        <tr>
                                            {{--<th class="text-center">--}}
                                                {{--<svg class="c-icon">--}}
                                                    {{--<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-people"></use>--}}
                                                {{--</svg>--}}
                                            {{--</th>--}}
                                            <th>Member</th>
                                            <th class="text-center">Email</th>
                                            <th>Mobile</th>
                                            <th class="text-center">Payment Method</th>
                                            <th>Porcessed Status</th>
                                            <th>Registered date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach ($registrations as $member)
                                            <tr>
                                                {{--<td class="text-center">--}}
                                                    {{--<div class="c-avatar">--}}
                                                        {{--<img class="c-avatar-img"--}}
                                                             {{--src="/storage/uploads/{{$member->oImage}}"--}}
                                                             {{--alt="user@email.com">--}}
                                                        {{--<span class="c-avatar-status bg-success"></span>--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                                <td>
                                                    <div>{{$member->txtFirstName}} {{$member->txtLastName}}</div>
                                                    {{--<div class="small text-muted"><span>New</span> | Registered: Jan 1,--}}
                                                        {{--2015--}}
                                                    {{--</div>--}}
                                                </td>
                                                <td class="text-center">
                                                    <div>
                                                        {{$member->txtEmail}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        {{$member->txtMobileNum}}
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <svg class="c-icon c-icon-xl">
                                                        <use xlink:href="vendors/@coreui/icons/svg/brand.svg#cib-cc-mastercard"></use>
                                                    </svg>
                                                </td>
                                                <td>
                                                    <div>
                                                        {{$member->processed}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        {{$member->created_at}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                                       aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{route('registration-edit',['id'=>$member->id])}}">
                                                            Edit
                                                        </a>
                                                        @can('isAdmin')
                                                        <a class="dropdown-item" onclick="confirmDelete({{$member->id}})"
                                                        >
                                                            Delete
                                                        </a>
                                                        @endcan
                                                    </div
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                    <div class="my-lg-4">{{ $registrations->links() }}</div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- /.row-->


                </div>
            </div>
        </main>

    </div>
@endsection