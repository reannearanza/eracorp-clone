<?php

namespace App\Http\Controllers;

use App\Jobs\SavePromocode;
use App\Log;
use App\Notification;
use App\Order;
use App\Payment;
use App\Promocode;
use App\User;
use App\Util;
use Illuminate\Http\Request;

class PaymentsController extends Controller
 {
    function index( Request $request ){

        $_email = $request->get( 'sEmail');

        $promocodeUsed = $request->get('sPromoCode');

        if($promocodeUsed === 'GCASHTEST' || $promocodeUsed === 'BPITEST') {
            $_amount = 5;
        }else{
            $_amount = ( int )$request->get('sTotalAmount');
        }

        $order_id = uniqid ( rand (), true );

        $user  = User::where('email',$_email)->get();
        $order  = new Order;
        $order->amount_paid = $_amount;
        $order->promocode = $promocodeUsed;
        $order->discount_percent = Promocode::getPercentByPromoCode($promocodeUsed);
        $order->reference_id = $order_id;
        $order->email = $_email;
        $order->status = 'pending'; //
        if($user){
            try{
                $user = $user->first();
                Log::write($user);
                $user->orders()->save($order);
            }catch(\Exception $e){
                Log::write($e->getMessage());
            }
        }

        // paynamics endpoint
        $wpf_url = env( 'PAYNAMICS_ENDPOINT', 'https://payin.paynamics.net/paygate/transactions/' );

        $mkey = env( 'PAYNAMICS_MERCHANT_KEY', '2A7234EB9390EDA5169997D2DDF1D4DB' );
        $_mid = env( 'PAYNAMICS_MERCHANT_ID', '000000080520B71AB1E4' );
        $_cancelurl  = 'https://eracorp.ph/payments/cancel';
        $_mtac_url   = 'https://eracorp.ph/privacy.html';
        $_descriptor = 'Eracorp';
        $_mobile     =  $request->get( 'sMobileNum' );
        // from step 1
        $_mlogo_url  = 'https://eracorp.ph/img/img-logo01.png';
        $_requestid = $order_id;
        // order id
        $_ipaddress = env('PAYNAMICS_MERCHANT_IP','103.125.216.92');
        $_noturl = env( 'PAYNAMICS_NOTIFICATION_URL', 'https://aranza.host/payments/notification' );
        // create route for this
        $_resurl = env( 'PAYNAMICS_REDIRECT_URL', 'https://aranza.host/payments/checkout' );
        // create route for this

        $_fname = $request->get( 'sFirstName' );
        // first name // from step 1
        $_lname = $request->get( 'sLastName' );
        // last name // from step 1
        $_mname = $request->get( 'sMiddleName', '' );
        // middle name
        $_addr1 = $request->get( 'sStreetAddress' );
        // street
        $_addr2 = $request->get( 'sAddress', '' );
        // address from step 1
        $_city = $request->get( 'sTownCity' );
        // town / city
        $_state = $request->get( 'sStreetAddress' );
        // province
        $_country = 'Philippines';
        $_zip = $request->get( 'sZipCode' );
        //
        $_email = $request->get( 'sEmail', 'example@email.com' );
        // from step 1
        $_phone = $request->get( 'sMobileNum' );

        $_clientip = $this->getRealIpAddr();

        // paynamics seems not accepting fractions of a peso, e.g 1904.40
        $_currency = 'PHP';
        $_sec3d = 'try3d';

        $forSign = $_mid .
        $_requestid .
        $_ipaddress .
        $_noturl .
        $_resurl .
        $_fname .
        $_lname .
        $_mname .
        $_addr1 .
        $_addr2 .
        $_city .
        $_state .
        $_country .
        $_zip .
        $_email .
        $_phone .
        $_clientip .
        number_format( ( $_amount ), 2, '.', $thousands_sep = '' ) .
        $_currency .
        $_sec3d;

        $cert = $mkey;
        //<-- your merchant key

        $_sign = hash( 'sha512', $forSign . $cert );

        $strxml = '';
        $strxml = $strxml . '<?xml version=\'1.0\' encoding=\'utf-8\' ?>';
        $strxml = $strxml . '<Request>';
        $strxml = $strxml . '<orders>';
        $strxml = $strxml . '<items>';
        $strxml = $strxml . '<Items>';
        $strxml = $strxml . '<itemname>Payment for #' . $order_id . '</itemname><quantity>1</quantity><amount>' . number_format( $_amount, 2, '.', $thousands_sep = '' ) . '</amount>';
        $strxml = $strxml . '</Items>';

        $strxml = $strxml . '</items>';
        $strxml = $strxml . '</orders>';
        $strxml = $strxml . '<mid>' . $_mid . '</mid>';
        $strxml = $strxml . '<request_id>' . $_requestid . '</request_id>';
        $strxml = $strxml . '<ip_address>' . $_ipaddress . '</ip_address>';
        $strxml = $strxml . '<notification_url>' . $_noturl . '</notification_url>';
        $strxml = $strxml . '<response_url>' . $_resurl . '</response_url>';
        $strxml = $strxml . '<cancel_url>' . $_cancelurl . '</cancel_url>';
        $strxml = $strxml . '<mtac_url>' . $_mtac_url . '</mtac_url>';
        // pls set this to the url where your terms and conditions are hosted
        $strxml = $strxml . '<descriptor_note>' . $_descriptor . '</descriptor_note>';
        // pls set this to the descriptor of the merchant
        $strxml = $strxml . '<fname>' . $_fname . '</fname>';
        $strxml = $strxml . '<lname>' . $_lname . '</lname>';
        $strxml = $strxml . '<mname>' . $_mname . '</mname>';
        $strxml = $strxml . '<address1>' . $_addr1 . '</address1>';
        $strxml = $strxml . '<address2>' . $_addr2 . '</address2>';
        $strxml = $strxml . '<city>' . $_city . '</city>';
        $strxml = $strxml . '<state>' . $_state . '</state>';
        $strxml = $strxml . '<country>' . $_country . '</country>';
        $strxml = $strxml . '<zip>' . $_zip . '</zip>';
        $strxml = $strxml . '<secure3d>' . $_sec3d . '</secure3d>';
        $strxml = $strxml . '<trxtype>sale</trxtype>';
        $strxml = $strxml . '<email>' . $_email . '</email>';
        $strxml = $strxml . '<phone>' . $_phone . '</phone>';
        $strxml = $strxml . '<mobile>' . $_mobile . '</mobile>';
        $strxml = $strxml . '<client_ip>' . $_clientip . '</client_ip>';
        $strxml = $strxml . '<amount>' . number_format( ( $_amount ), 2, '.', $thousands_sep = '' ) . '</amount>';
        $strxml = $strxml . '<currency>' . $_currency . '</currency>';
        $strxml = $strxml . '<mlogo_url>' . $_mlogo_url . '</mlogo_url>';
        // pls set this to the url where your logo is hosted
        $strxml = $strxml . '<pmethod></pmethod>';
        $strxml = $strxml . '<signature>' . $_sign . '</signature>';
        $strxml = $strxml . '</Request>';
        $b64string = base64_encode( $strxml );
        // lets log it for now for debugging purpose
        Log::write($b64string);

        return  ['paymentrequest'=>$b64string];
    }

    public function checkout( Request $request )
 {
        $notification = new Notification;
        $notification->post = json_encode( [
            'from'=>'checkout',
            'all'=>$request->all(),
            'content'=>$request->getContent(),
            'post'=>$request->post(),
            'request'=>$_REQUEST,
        ] );
        $notification->save();

        return view( 'checkout' );
    }

    public function notification( Request $request )
 {
        $paymentResponse = $request->get( 'paymentresponse', '' );
        if ( $paymentResponse ) {

            $xml =  base64_decode( $paymentResponse );
            preg_match( '/\<response_code\>.+<\/response_code\>/', $xml, $match );
            $paymentResponseCode = isset( $match[0] ) ? $match[0] : null ;
            $notification = new Notification;
            $notification->post = json_encode( ['paymentresponse'=>$paymentResponse, 'paymentResponseCode'=>$paymentResponseCode] );
            $result = $notification->save();

            // update the order status
            $xml = Util::decodeBase64($paymentResponse);
            $sx = simplexml_load_string($xml);
            $reqId = (string) $sx->application->request_id;
            $resId = (string) $sx->application->response_id;
            $resCode = (string) $sx->responseStatus->response_code;
            $paymentMethod = (string) $sx->application->ptype;

            $order = Order::where('reference_id',$reqId)->get();

            $notification2 = new Notification;

            if( $order = $order->first() ){
                $order->response_code = $resCode;
                $order->response_id = $resId;
                //$order->discount_percent = Promocode::getPercentByPromoCode($reqId);
                $order->status =  $resCode == 'GR001' ? 'Successful payment' : 'Payment failed';
                $order->payment_method = $paymentMethod;
                $order->save();

                // lets update payments table

                $user = User::find($order->user_id);
                $payment = new Payment();
                $payment->user_id = $user->id;
                $payment->email = $user->email;
                $payment->amount_paid = $order->amount_paid;
                $payment->discount_code = $order->promocode;
                $payment->discount_percent = Promocode::getPercentByPromoCode($order->promocode);
                $payment->method = 'Online Payment : ' . $sx->application->ptype;
                $payment->company = Promocode::getCompanyByPromoCode($order->promocode); 
                $payment->status = $order->status;
                $result =  $payment->save();


                $notification2->post = json_encode([ 'order'=>$order ]);
            }else{
                $notification2->post = json_encode([ 'order'=> $order,'note'=>'else']);
            }
            
            $notification2->save();



            
            

            return [
                'notification' => json_decode( $notification->post ),
                'result'=>$result,
            ];
        }
    }

    public function cancel( Request $request ) {

        $notification = new Notification;
        $notification->post = json_encode( $request->all() );
        $result = $notification->save();
        //            return [
        //                'notification' => $notification->post,
        //                'result'=>$result,
        //            ];
        return '<h1 style="text-align: center">Cancel Page Place Holder</h1>';
    }

    public function process( Request $request ) {
        // this is for now, will link to actual payment processsor.
        return json_encode( $request->all() );
    }

    public function viaCompany( Request $request ) {
        try {

            $user = User::where('email',$request->get('email'))->get()->first();
            $payment = new Payment();
            $payment->user_id = $user->id;
            $payment->email = $request->get( 'email' );
            $payment->amount_paid = $request->get( 'amount_paid' );
            $payment->discount_code = $request->get( 'discount_code' );
            $payment->discount_percent = $request->get( 'discount_percent' );
            $payment->method = $request->get( 'method' );
            $payment->company = $request->get( 'company', '' ); 
            $payment->status = $request->get( 'status', 'pending' );
            $result =  $payment->save();

            if ( $result ) {
                return [
                    'user'=> $user->name,
                    'email'=>$payment->email,
                    'discount_code'=>$payment->discount_code,
                    'success'=>true
                ];
            }

            

        } catch( \Exception $e ) {
            return ['error'=>$e->getMessage()];
        }
    }

    public function viaCompanyResultPage( Request $request ) {
        
        SavePromocode::dispatch($request->get('email'),$request->get('discount_code'))->delay(2);

        return view( 'payments.via-company-result', ['data'=>$request->all()] );
    }

    private function getRealIpAddr(){
        if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ){
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] )){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
