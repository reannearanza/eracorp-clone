@extends('layouts.admin')
@section('content')

    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-12">
                            <system-users :users="{{$users}}"></system-users>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
