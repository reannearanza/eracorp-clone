<?php

namespace App\Jobs;

use App\Registration;
use App\User;
use App\Util;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage; 

class CreateUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $registrant;
    protected $password;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Registration $registrant,$password)
    {
        $this->registrant = $registrant;
        //Storage::put('pw.txt', print_r($password, true));
        $this->password   =  bcrypt($password);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = new User();
        $user->password = $this->password;
        $user->email = $this->registrant->txtEmail;
        $user->name = $this->registrant->txtFirstName . " " .$this->registrant->txtLastName;
        $user->user_type = 'member';
        $user->profile_id = $this->registrant->id;
        $user->save();
    }
}
