<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/contact-us/store','ContactUsController@store');
Route::get('/contact-us','ContactUsController@index');
Route::post('/contact-us/reply','ContactUsController@reply');

Route::post('/registration/store','RegistrationController@store');
Route::post('/registration/email', 'RegistrationController@email');
Route::get('/registration','RegistrationController@index');
