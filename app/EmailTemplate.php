<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplates extends Model
{
    public function index(){
        return view('email-templates.index');
    }
}
