<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    
    public static function getPercentByPromoCode($code){
        $code = Promocode::where('code',$code)->get();
        return $code->count() ? $code->first()->percent : 0 ;
    }

    public static function getCompanyByPromoCode($code){
        $code = Promocode::where('code',$code)->get();
        return $code->count() ? $code->first()->company : null ;
    }
}
