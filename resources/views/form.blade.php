@extends('layouts.app')
@section('content')
    <div class="register">
        <div class="container py-5">
            <div class="row" id="app">
                <div class="col-md-3 register-left">
                    <img src="img/img-logo01.png" alt="Logo"/>
                    <h4>Welcome</h4>
                    <p>Already have an account?</p>
                    <p><a class="btn btn-light" href="/login">LOGIN</a></p>
                </div>
                <div class="col-md-9 register-right d-flex flex-column justify-content-center">
                    <div class="register-form" v-if="!showSuccessMessage && !loadPaymentForm">
                        <h3>ERA Registration</h3>
                    </div>
                    <div>
                        <form role="form" v-if="showSuccessMessage==false">
                            <div class="register-form" id="register-form" style="padding-top:0;">
                                <!-- STEP 1 -->
                                {{--<div class="modal" id="cropperModal" tabindex="1" role="dialog"--}}
                                     {{--aria-labelledby="exampleModalCenterTitle" aria-hidden="false">--}}
                                    {{--<div class="modal-dialog modal-dialog-centered" role="document">--}}
                                        {{--<div class="modal-content">--}}
                                            {{--<div class="modal-header">--}}
                                                {{--<h5 class="modal-title" id="exampleModalLongTitle">Crop Image</h5>--}}
                                                {{--<button type="button" class="close btnCancel" data-dismiss="modal"--}}
                                                        {{--aria-label="Close">--}}
                                                    {{--<span aria-hidden="true">&times;</span>--}}
                                                {{--</button>--}}
                                            {{--</div>--}}
                                            {{--<div class="modal-body" id="cropperModalBody"--}}
                                                 {{--style="max-height: 25em; overflow:hidden; padding:0; width: auto;">--}}
                                            {{--</div>--}}
                                            {{--<div class="modal-footer">--}}
                                                {{--<button type="button" class="btn btn-secondary btnCancel"--}}
                                                        {{--data-dismiss="modal">Close--}}
                                                {{--</button>--}}
                                                {{--<button type="button" class="btn btn-primary" id="btnCropImage">Save--}}
                                                    {{--changes--}}
                                                {{--</button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div v-if="iPage == 1">
                                    <h5>Personal Information</h5>
                                    <div class="row">
                                        {{--<div class="col-xl-3 col-lg-4 order-lg-2 col-md-5 order-md-2">--}}
                                            {{--<div class="form-group filePhotoDiv">--}}
                                                {{--<vue-dropzone ref="filePhoto" class="fileUploader" id="filePhoto"--}}
                                                              {{--:options="dropzoneOptions"--}}
                                                              {{--@vdropzone-file-added="addedFile"--}}
                                                              {{--@vdropzone-error="dropzoneError"></vue-dropzone>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-6">
                                            <div class="form-group pb-md-1">
                                                <input type="text" class="form-control" id="txtFirstName"
                                                       v-model="oForm[1].sFirstName" v-on:input="validateRequired"
                                                       placeholder="First Name *">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group pb-md-1">
                                                <input type="text" class="form-control" id="txtLastName"
                                                       v-model="oForm[1].sLastName" v-on:input="validateRequired"
                                                       placeholder="Last Name *">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="txtEmail"
                                                       v-model="oForm[1].sEmail" v-on:input="validateEmail" v-on:blur="checkEmail"
                                                       placeholder="Email address *">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <masked-input v-model="oForm[1].sBirthdate" mask="11/11/1111"
                                                              placeholder="Birthdate * (mm/dd/yyyy)"
                                                              class="form-control" v-on:input="validateBirthdate"
                                                              id="txtBirthdate"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtMobileNum"
                                                       v-model="oForm[1].sMobileNum" v-on:input="validateNumber"
                                                       placeholder="Mobile number *">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtHomeAddress"
                                                       v-model="oForm[1].sHomeAddress" v-on:input="validateRequired"
                                                       placeholder="Home address *">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtCodeword"
                                                       v-model="oForm[1].sCodeWord" v-on:input="validateRequired"
                                                       placeholder="Codeword (Used to verify your identify during alarm) *">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="mobile_type" class="form-control"
                                                        v-model="oForm[1].sMobileType" id="txtMobileType"
                                                        v-on:input="validateRequired">
                                                    <option :value="null">Select your Mobile Phone Type</option>
                                                    <option value="iOS">iOS</option>
                                                    <option value="Android">Android</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="txtSpecificCond"
                                                       v-model="oForm[1].sSpecificCond"
                                                       placeholder="Specific Condition for Alarm Responding">
                                            </div>
                                        </div>
                                        <div id="emergencyContacts" class="col-md-12">
                                            <h5>Emergency Contacts:</h5>
                                            <div class="row emergencyContact"
                                                 v-for="(emergContact, index) in oForm[1].aEmergContacts">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input v-model="emergContact.sEFullName"
                                                               v-on:input="validateRequired" type="text"
                                                               class="form-control"
                                                               placeholder="FullName (Emergency Contact Person)"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input v-model="emergContact.sEMobileNum"
                                                                   v-on:input="validateNumber" type="text"
                                                                   class="form-control"
                                                                   placeholder="Mobile number (Emergency Contact Person)"/>
                                                            <div class="input-group-append">
                                                                <button v-on:click.prevent="addContact"
                                                                        class="btn btn-primary col btnAddContact"><i
                                                                            class="fas fa-plus"></i></button>
                                                                <button v-on:click.prevent="removeContact"
                                                                        class="btn btn-primary ml-1 col btnRemoveContact"
                                                                        hidden><i
                                                                            class="fas fa-minus"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-check">
                                                <input v-model="bAgreedTAC" class="form-check-input" type="checkbox" value="" id="termsandconditions">
                                                <label class="form-check-label" for="termsandconditions">
                                                    I read and agree to the Eracorp's <a href="/terms" target="_blank">Terms and Conditions</a>.
                                                </label>
                                                <div v-if="!bAgreedTAC" class="invalid-feedback d-block">You must agree to the terms and conditions to register an account.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- STEP 2 -->
                                {{--<div v-if="iPage == 2">--}}
                                    {{--<h5>Health Insurance Information (if any)</h5>--}}
                                    {{--<p class="mb-5">This will help us collect medical emergency information and--}}
                                        {{--respondents to contact.</p>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtMedical"--}}
                                                       {{--v-model="oForm[2].sMedicalCond" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Any medical condition to note *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtProvider"--}}
                                                       {{--v-model="oForm[2].sHealthProvider" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Health care provider *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtPolicyNum"--}}
                                                       {{--v-model="oForm[2].sPolicyNum" v-on:input="validateNumber"--}}
                                                       {{--placeholder="Policy *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtHealthPlan"--}}
                                                       {{--v-model="oForm[2].sHealthPlan" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Type of health plan *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <!-- STEP 3 -->
                                <div v-if="loadPaymentForm" style="height:100%;">
                                    <circle2 size="8em" style="margin:auto;"></circle2>
                                    <h5 class="mt-4 text-center">Loading...</p>
                                </div>
                                {{--<div v-if="iPage === 3">--}}
                                    {{--<h5>Vehicle to be registered</h5>--}}
                                    {{--<p class="mb-5">You can register 1 car as part of our services.</p>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtCarColor"--}}
                                                       {{--v-model="oForm[3].sCarColor" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Car Color *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtCarMake"--}}
                                                       {{--v-model="oForm[3].sCarMake" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Car Make *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtYearModel"--}}
                                                       {{--v-model="oForm[3].sYearModel" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Year Model *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input type="text" class="form-control" id="txtPlateNum"--}}
                                                       {{--v-model="oForm[3].sPlateNum" v-on:input="validateRequired"--}}
                                                       {{--placeholder="Plate Number *">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <!-- BUTTONS -->
                                {{--<div class="form-group text-center pt-3" v-if="!loadPaymentForm">--}}
                                    {{--<button class="btn px-4 btn-primary" v-if="iPage != 1"--}}
                                            {{--@click.prevent="previousPage">Previous--}}
                                    {{--</button>--}}
                                    {{--<button class="btn px-4 btn-primary" v-if="iPage < iTotalPages"--}}
                                            {{--@click.prevent="nextPage">Next--}}
                                    {{--</button>--}}
                                <div v-if="!submitted" class="text-right">
                                    <button  class="btn px-4 btn-primary"
                                             @click.prevent="submit" type="submit">Submit
                                    </button>
                                </div>

                                {{--</div>--}}
                            </div>
                            {{--<p class="text-right mt-4" v-if="iPage != null"><strong>Page @{{ iPage + ' of ' + iPage--}}
                                    {{--}}</strong></p>--}}
                        </form>
                        <div v-if="!isPaidByCompany">
                            <div v-if="showBillingDetails " class="register-form" id="billing-form">
                                <div class="text-left mb-4 py-4">

                                    <h3>Payment Summary</h3>

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th colspan="2">Referal / Promo code</th>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td width="60%">
                                                <input v-model="promoCode" type="text" id="promoCode"
                                                       class="form-control m-2" placeholder="CODE">
                                                <div v-if="promocodeSuccess" class="alert alert-success ml-3">
                                                    @{{promocodeSuccess}}
                                                </div>
                                                <div v-if="promocodeError" class="alert alert-danger ml-3">
                                                    @{{promocodeError}}
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-block m-2"
                                                        @click.prevent="verifyCode(promoCode)">
                                                    <i class="fas fa-money-check mr-2"></i>
                                                    Enter Code
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table p-4">
                                        <tbody>
                                        <tr>
                                            <td width="80%">Annual Registration Fee</td>
                                            <td><input type="text" name="annualFee" v-model="currency(membershipPrice)"
                                                       class="border-0 text-right" placeholder="0.00"></td>
                                        </tr>
                                        <tr>
                                            <td width="80%">+ Vat</td>
                                            <td><input type="text" name="vat" v-model="currency(vatPrice)"
                                                       class="border-0 text-right" placeholder="+ 0.00"></td>
                                        </tr>
                                        <tr>
                                            <td width="80%">Promo discount</td>
                                            <td><input type="text" name="promoDiscount" v-model="currency(promoPrice)"
                                                       class="border-0 text-right" placeholder="- 0.00"></td>
                                        </tr>
                                        <tr>
                                            <td width="80%" style="text-align: right">Total</td>
                                            <td><input type="text" class="border-0 text-right"
                                                       v-model="currency(totalPrice)" placeholder="0.00"></td>
                                        </tr>
                                        </tbody>

                                    </table>

                                    <div id="accordion" class="mt-3">
                                        <div class="card border-0" style="border-radius: unset">
                                            <div class="p-2 " id="headingOne">
                                                {{--<h5 class="mb-0">--}}
                                                <input class="input-group-lg" data-toggle="collapse"
                                                       data-target="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne" type="radio" name="payment-type"
                                                       value="online_payment">
                                                <label class="form-check-label" for="exampleRadios1">
                                                    Bank Payment
                                                </label>
                                                {{--</h5>--}}
                                            </div>

                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion">
                                                <div class="card-body">
                                                    Security Bank:<br/>
                                                    Account name - Home Defense Technology Corp<br/>
                                                    Account number - 0000008620991<br/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card border-0" style="border-radius: unset">
                                            <div class="p-2" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <input class="input-group-lg" data-toggle="collapse"
                                                           data-target="#collapseTwo" aria-expanded="true"
                                                           aria-controls="collapseOne" type="radio" name="payment-type"
                                                           value="online_payment">
                                                    <label class="form-check-label" for="exampleRadios1">
                                                        Online Payment
                                                    </label>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                                 data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="alert alert-dark">
                                                        Your personal data will be used to process your order, support
                                                        your
                                                        experience throughout this website, and for other
                                                        purposes described in our <a href="{{ route('privacy') }}"><span
                                                                    class="text-info">privacy policy</span></a>.
                                                    </div>

                                                    <div>
                                                        <h2 class="p-2">Billing details</h2>
                                                        <div class="row" id="billingForm">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="txtBillingFirstName">First name
                                                                        *</label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sFirstName"
                                                                           id="txtBillingFirstName"
                                                                           class="form-control" placeholder="First name"
                                                                           v-on:input="validateRequired">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="txtBillingMiddleName">Middle
                                                                        name</label>
                                                                    <input type="text"
                                                                           id="txtBillingMiddleName"
                                                                           class="form-control"
                                                                           placeholder="Midlle name"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="txtBillingLastName">Last name *</label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sLastName"
                                                                           id="txtBillingLastName"
                                                                           class="form-control" placeholder="Last name"
                                                                           v-on:input="validateRequired">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="txtBillingCompany">Company
                                                                        (optional)</label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sCompany"
                                                                           id="txtBillingCompany"
                                                                           class="form-control"
                                                                           placeholder="Company">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="txtBillingStreetAddress">Street address
                                                                        *</label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sStreetAddress"
                                                                           id="txtBillingStreetAddress"
                                                                           class="form-control"
                                                                           placeholder="Street address *"
                                                                           v-on:input="validateRequired">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="txtBillingTownCity">Town / City
                                                                        * </label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sTownCity"
                                                                           id="txtBillingTownCity"
                                                                           class="form-control"
                                                                           placeholder="Town / City"
                                                                           v-on:input="validateRequired">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label for="txtBillingProvince">Province * </label>
                                                                <div class="form-group">
                                                                    <select name="billing_state" class="form-control"
                                                                            v-model="oBillingDetails.sProvince"
                                                                            id="txtBillingProvince"
                                                                            v-on:input="validateRequired">
                                                                        <option value="">Select your province</option>
                                                                        <option value="ABR">Abra</option>
                                                                        <option value="AGN">Agusan del Norte</option>
                                                                        <option value="AGS">Agusan del Sur</option>
                                                                        <option value="AKL">Aklan</option>
                                                                        <option value="ALB">Albay</option>
                                                                        <option value="ANT">Antique</option>
                                                                        <option value="APA">Apayao</option>
                                                                        <option value="AUR">Aurora</option>
                                                                        <option value="BAS">Basilan</option>
                                                                        <option value="BAN">Bataan</option>
                                                                        <option value="BTN">Batanes</option>
                                                                        <option value="BTG">Batangas</option>
                                                                        <option value="BEN">Benguet</option>
                                                                        <option value="BIL">Biliran</option>
                                                                        <option value="BOH">Bohol</option>
                                                                        <option value="BUK">Bukidnon</option>
                                                                        <option value="BUL">Bulacan</option>
                                                                        <option value="CAG">Cagayan</option>
                                                                        <option value="CAN">Camarines Norte</option>
                                                                        <option value="CAS">Camarines Sur</option>
                                                                        <option value="CAM">Camiguin</option>
                                                                        <option value="CAP">Capiz</option>
                                                                        <option value="CAT">Catanduanes</option>
                                                                        <option value="CAV">Cavite</option>
                                                                        <option value="CEB">Cebu</option>
                                                                        <option value="COM">Compostela Valley</option>
                                                                        <option value="NCO">Cotabato</option>
                                                                        <option value="DAV">Davao del Norte</option>
                                                                        <option value="DAS">Davao del Sur</option>
                                                                        <option value="DAC">Davao Occidental</option>
                                                                        <option value="DAO">Davao Oriental</option>
                                                                        <option value="DIN">Dinagat Islands</option>
                                                                        <option value="EAS">Eastern Samar</option>
                                                                        <option value="GUI">Guimaras</option>
                                                                        <option value="IFU">Ifugao</option>
                                                                        <option value="ILN">Ilocos Norte</option>
                                                                        <option value="ILS">Ilocos Sur</option>
                                                                        <option value="ILI">Iloilo</option>
                                                                        <option value="ISA">Isabela</option>
                                                                        <option value="KAL">Kalinga</option>
                                                                        <option value="LUN">La Union</option>
                                                                        <option value="LAG">Laguna</option>
                                                                        <option value="LAN">Lanao del Norte</option>
                                                                        <option value="LAS">Lanao del Sur</option>
                                                                        <option value="LEY">Leyte</option>
                                                                        <option value="MAG">Maguindanao</option>
                                                                        <option value="MAD">Marinduque</option>
                                                                        <option value="MAS">Masbate</option>
                                                                        <option value="MSC">Misamis Occidental</option>
                                                                        <option value="MSR">Misamis Oriental</option>
                                                                        <option value="MOU">Mountain Province</option>
                                                                        <option value="NEC">Negros Occidental</option>
                                                                        <option value="NER">Negros Oriental</option>
                                                                        <option value="NSA">Northern Samar</option>
                                                                        <option value="NUE">Nueva Ecija</option>
                                                                        <option value="NUV">Nueva Vizcaya</option>
                                                                        <option value="MDC">Occidental Mindoro</option>
                                                                        <option value="MDR">Oriental Mindoro</option>
                                                                        <option value="PLW">Palawan</option>
                                                                        <option value="PAM">Pampanga</option>
                                                                        <option value="PAN">Pangasinan</option>
                                                                        <option value="QUE">Quezon</option>
                                                                        <option value="QUI">Quirino</option>
                                                                        <option value="RIZ">Rizal</option>
                                                                        <option value="ROM">Romblon</option>
                                                                        <option value="WSA">Samar</option>
                                                                        <option value="SAR">Sarangani</option>
                                                                        <option value="SIQ">Siquijor</option>
                                                                        <option value="SOR">Sorsogon</option>
                                                                        <option value="SCO">South Cotabato</option>
                                                                        <option value="SLE">Southern Leyte</option>
                                                                        <option value="SUK">Sultan Kudarat</option>
                                                                        <option value="SLU">Sulu</option>
                                                                        <option value="SUN">Surigao del Norte</option>
                                                                        <option value="SUR">Surigao del Sur</option>
                                                                        <option value="TAR">Tarlac</option>
                                                                        <option value="TAW">Tawi-Tawi</option>
                                                                        <option value="ZMB">Zambales</option>
                                                                        <option value="ZAN">Zamboanga del Norte</option>
                                                                        <option value="ZAS">Zamboanga del Sur</option>
                                                                        <option value="ZSI">Zamboanga Sibugay</option>
                                                                        <option value="NCR">Metro Manila</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="txtBillingZip">Zip Code * </label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sZipCode"
                                                                           id="txtBillingZip"
                                                                           class="form-control" placeholder="Zip Code"
                                                                           v-on:input="validateRequired">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="txtBillingMobileNum">Mobile Number
                                                                        * </label>
                                                                    <input type="text"
                                                                           v-model="oBillingDetails.sMobileNum"
                                                                           class="form-control"
                                                                           placeholder="Mobile Phone *"
                                                                           v-on:input="validateNumber"
                                                                           id="txtBillingMobileNum">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-block btn-danger mt-4"
                                                            @click.prevent="submitBilling">
                                                        Pay via Paynamics
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-else>
                            <div class="text-center align-self-center alert alert-success p-4">
                                Your payment will be processed by your company.
                                <button class="btn btn-danger mx-3" @click.prevent="byPassPayment({
                                    discount_code: promoCode,
                                    discount_percent: promoDiscount,
                                    amount_paid: totalPrice,
                                    method: 'via-company',
                                    amount_paid:totalPrice,
                                    email: oForm[1].sEmail,
                                    company: company
                                })">Click to Proceed
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div v-show="showPaynamicsButton" class="col-md-12 p-5">
                                <form action="{{env('PAYNAMICS_ENDPOINT')}}" method="POST"
                                      id="paymentConfirmation">
                                    <div class="card my-4" v-show="!loadPayment">
                                        <div class="card-body">
                                            <input type="hidden"
                                                   v-model="paynamicsHashCode"
                                                   name="paymentrequest"
                                                   class="form-control my-4" placeholder="Hash value goes here">
                                            <p class="card-text">
                                                You are about to pay <span class="font-weight-bolder">@{{currency(totalPrice)}}</span>
                                                to Home Defense Technology Corp via Paynamics.
                                            </p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="submit" @click.prevent="proceedPayment" href="#"
                                                            class="btn btn-danger btn-block">Proceed to payment
                                                    </button>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="#" class="btn btn-primary btn-block">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div v-if="loadPayment" style="height:100%;">
                                    <gauge style="transform: scale(5); height:100%; margin:auto;"></gauge>
                                    <h5 class="mt-4 text-center">Loading...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <!-- SCRIPT -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <script src="{{ asset('js/registration-form.js') }}" type="module"></script>
@endsection
