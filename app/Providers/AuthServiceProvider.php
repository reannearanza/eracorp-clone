<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Gate $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('isAdmin',function($user){
            return $user->user_type == 'admin';
        });
        $gate->define('isOperator',function($user){
            return $user->user_type == 'operator' || $user->user_type == 'admin';
        });
        $gate->define('isMember',function($user){
            return $user->user_type == 'member';
        });
    }
}
