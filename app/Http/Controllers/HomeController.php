<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Test upload image
     */
    public function upload(Request $oRequest)
    {
        if($oRequest->file('file'))
        {
           $image = $oRequest->file('file');
           $name = time().$image->getClientOriginalName();
           $image->move(public_path().'/images/', $name); 
         }
 
        $image= new Image();
        $image->image_name = $name;
        $image->save();
 
        return response()->json(['success' => 'You have successfully uploaded an image'], 200);
    }
}
