@extends('layouts.app')
@section('content')

<div class="payment-success">
    <div class="container py-5">
        <div class="row py-5 justify-content-center">
            <div class="col-md-10 offset-md-1">
                <div class="card">
                    <div class="card-body text-center py-5">
                        <h1 class="mb-5"><strong>Emergency Rapid Assistance Corporation</strong></h1>
                        <h2 class="mb-4">Your Payment is Successful!</h2>
                        <p class="mb-4">Thank you for your payment.<br>
                            An automated payment receipt will be sent to your registered email.</p>
                        <a href="/" class="btn btn-primary">Back to Home</a>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
</div>

@endsection

