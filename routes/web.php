<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

/**
 * quick & dirthy fix for user type redirectiono on password reset
 */
Route::any('/go', function () {
    $user = auth()->user();
    $redirect = $user->user_type == 'member' ? '/member' : '/admin';
    return redirect($redirect);
});

Route::post('/upload', 'HomeController@index');

Route::get('/registration', function () {
    return view('form');
});

Route::get('/about-us', function () {
    return redirect('/#about-us');
});

Route::get('/privacy.html', function () {
    return view('privacy');
})->name('privacy');

Route::get('/terms', function () {
    return view('terms');
});

Route::any('/payments', 'PaymentsController@index');
Route::any('/payments/notification', 'PaymentsController@notification');
Route::any('/payments/checkout', 'PaymentsController@checkout');
Route::post('/payments/process', 'PaymentsController@process');
Route::post('/payments/via-company','PaymentsController@viaCompany');
Route::any('/payments/via-company-result','PaymentsController@viaCompanyResultPage');
Route::any('/payments/cancel','PaymentsController@cancel');



Route::get('/admin', 'AdminController@index')->name('admin-index');
Route::get('/admin/contact-us', 'AdminController@contactUs');
Route::get('/admin/chat', 'AdminController@chat');
Route::get('/admin/paynamics', 'AdminController@paynamics');

Route::get('/admin/promocodes', 'PromocodeController@index')->name('promocodes-index');
Route::get('/admin/promocodes/create', 'PromocodeController@create')->name('promocodes-create');
Route::post('/admin/promocodes/save', 'PromocodeController@store')->name('promocodes-save');
Route::get('/admin/promocodes/edit/{id}', 'PromocodeController@edit')->name('promocodes-edit');
Route::get('/admin/promocodes/delete/{id}', 'PromocodeController@destroy')->name('promocodes-delete');
Route::post('/admin/promocodes/update/{id}', 'PromocodeController@update')->name('promocodes-update');
Route::any('/admin/promocodes/verify/{code}', 'PromocodeController@verify');


Route::get('/admin/payments', 'AdminController@payments')->name('admin-payments-index');
Route::get('/admin/payments/edit/{id}', 'AdminController@paymentsEdit')->name('admin-payments-edit');
Route::post('/admin/payments/update/{id}', 'AdminController@paymentsUpdate')->name('admin-payments-update');

Route::any('/admin/registration/edit/{id}', 'RegistrationController@edit')->name('registration-edit');
Route::post('/admin/registration/update/{id}', 'RegistrationController@update')->name('registration-update');
Route::any('/admin/registration/delete/{id}', 'RegistrationController@destroy')->name('registration-delete');
Route::any('/admin/registration/search', 'RegistrationController@search')->name('registration-delete');
Route::any('/admin/email-templates', 'EmailTemplatesController@index')->name('email-templates');
Route::get('/admin/users', 'UserController@index')->name('users-index');
Route::get('/admin/users/list', 'UserController@getUsers')->name('users-list');
Route::post('/admin/users/store', 'UserController@store')->name('users-store');
Route::post('/admin/users/delete/{id}', 'UserController@destroy');

Route::get('/admin/import-users', 'ImportUsersController@create');
Route::post('/admin/import-store', 'ImportUsersController@store');





Route::get('/member','MemberController@profile')->name('member');
Route::get('/member/contacts','MemberController@emergencyContacts');
Route::post('/member/save/profile/{id}','MemberController@saveProfile');
Route::post('/member/save/contacts/{id}','MemberController@saveContacts');
Route::post('/member/save/image/{id}','MemberController@saveImage');
Route::post('/member/save/health-info/{id}','MemberController@saveHealthInfo');
Route::post('/member/save/registered-vehicle/{id}','MemberController@saveRegisteredVehicle');
//Route::get('/member/profile','MemberController@profile');
Route::get('/member/terms','MemberController@terms');
Route::get('/member/billing','MemberController@billing');
Route::get('/member/order/{id}','MemberController@getOrder');
Route::get('/member/personal-info/{id}','MemberController@getPersonalInfo');



Route::get('/test', 'TestController@test');
Route::get('/mail-test', 'TestController@mail');
Route::get('/mail-template', 'TestController@mailtemplate');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

