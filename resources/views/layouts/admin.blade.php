<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v3.2.0
* @link https://coreui.io
* Copyright (c) 2020 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Eracorp Admin</title>
    <!-- Main styles for this application-->
    <link href="/admin_/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/fontawesome/css/all.css">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script>
        window.dataLayer = window.dataLayer || [];
    </script>
</head>
<body class="c-app flex-row align-items-center">
<div id="app" class="vw-100">
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
    </div>
    <ul class="c-sidebar-nav">
        @can('isOperator')
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin">
                    Registrations
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/contact-us">
                    Contact Us Messages
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/import-users">
                    Import Users
                </a>
            </li>
            <!-- <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/email-templates">
                    Email Templates
                </a>
            </li> -->
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/chat">
                    Live Chat
                </a>
            </li>
        @endcan
        @can('isAdmin')
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/payments">
                    Payments
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/promocodes">
                    Promo Codes
                </a>
            </li>

            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/admin/users">
                    Manage System Users
                </a>
            </li>

            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="https://ptibis.paynamics.net/paygate/default.aspx" target="_blank">
                    Paynamics
                </a>
            </li>
        @endcan
        @can('isMember')
            {{--<li class="c-sidebar-nav-item">--}}
            {{--<a class="c-sidebar-nav-link" href="/member">--}}
            {{--<svg class="c-sidebar-nav-icon">--}}
            {{----}}
            {{--</svg>--}}
            {{--Dashboard--}}
            {{--</a>--}}
            {{--</li>--}}

            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/member">
                    Personal Information
                </a>
            </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="/member/contacts">
                        Emergency Contacts
                    </a>
                </li>
        
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/member/billing">
                    Billing
                </a>
            </li>

            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/member/terms">
                    Terms and Conditions
                </a>
            </li>
        @endcan
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
</div>
<div class="c-wrapper c-fixed-components">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
                data-class="c-sidebar-show">
        </button>
        <!-- <a class="c-header-brand d-lg-none">ERACORP</a> -->
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
                data-class="c-sidebar-lg-show" responsive="true">
        </button>

        <ul class="c-header-nav ml-auto mr-4">

            <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link">
                </a></li>
            <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link">
                </a></li>
            <li class="c-header-nav-item d-md-down-none mx-2">
                <a class="c-header-nav-link">
                </a></li>
            <li class="c-header-nav-item dropdown">
                <a class="c-header-nav-link" data-toggle="dropdown"
                   role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog fa-lg"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right pt-0">
                    @can('isMember')
                    <a class="dropdown-item" href="/member">Personal Information</a>
                    <a class="dropdown-item" href="/member/contacts">Emergency Contacts</a>
                    <a class="dropdown-item" href="/member/billing">Billing</a>
                    @endcan
                    <a class="dropdown-item"
                       href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        Logout
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                </div>
            </li>
        </ul>
    </header>
    @yield('content')
</div>
</div>


<!-- CoreUI and necessary plugins-->
<script src="/admin_/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="/admin_/vendors/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]-->
<script src="/admin_/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
<script src="/admin_/vendors/@coreui/utils/js/coreui-utils.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function confirmDelete(id){
        var userId = id;
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    window.location="/admin/registration/delete/"+userId;
                } else {
                   // swal("Your imaginary file is safe!");
                }
                return false;
            });

    }
</script>

<!-- <script src="/admin_/js/main.js"></script> -->
</body>
</html>