<?php

namespace App\Http\Controllers;
use App\ContactUs;
use App\Registration;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        if(!\Gate::allows('isAdmin')){
//            abort(403,"Invalid user permission!");
//        }
    }

    public function index(Request $request)
    {
        $this->isOperator();
        $registrations = DB::table('registrations')->orderBy('id','desc')->paginate(15);
        return view('admin.dashboard',['registrations'=>$registrations]);
    }
    public function contactUs(Request $request)
    {
       $this->isOperator();
        $records = ContactUs::paginate(10);
        return view('admin.contact-us',['records'=>$records]);
    }
    public function chat(Request $request)
    {
        $this->isOperator();
        return view('admin.chat');
    }
    public function paynamics(Request $request)
    {
        $this->isAdmin();
        return view('admin.paynamics');
    }

    public function payments(){
        $this->isAdmin();

        $records = DB::table('payments')->orderBy('id','desc')->paginate(15);

        return view('admin.payments.index',['records'=>$records]);
    }

    public function paymentsEdit($id){
        $this->isAdmin();
        $payment = Payment::find($id);
        return view('admin.payments.edit',['payment'=>$payment]);
    }

    public function paymentsUpdate($id,Request $request){
        $this->isAdmin();
        $payment = Payment::find($id);
        $payment->status = $request->post('payment-status');
        $payment->save();
        return redirect()->route('admin-payments-index');
    }


    private function isAdmin(){
        if(!\Gate::allows('isAdmin')){
            abort(403,"Invalid user permission!");
        }
    }
    private function isOperator(){
        if(!\Gate::allows('isOperator')){
            abort(403,"Invalid user permission!");
        }
    }
}
