<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'email',
        'amount_paid',
        'discount_code',
        'discount_percent',
        'method'
    ];
}
