<?php

namespace App\Http\Controllers;

use App\Promocode;
use Illuminate\Http\Request;

class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Promocode::paginate(6);
        return view('promocodes.index',['records'=>$records]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promocodes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //@Todos, add validations

        $promocode = new Promocode();
        $promocode->code = $request->get('code');
        $promocode->company = $request->get('company',"");
        $promocode->percent = $request->get('percent');
        $promocode->expiration = $request->get('expiration');
        $promocode->save();
        return redirect()->route('promocodes-index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code = Promocode::find($id);
        $data = $code->toArray();
        return view('promocodes.edit',['code'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = $request->all();
            $promocode = Promocode::find($id);
            $promocode->code = $request->get('code');
            $promocode->company = $request->get('company');
            $promocode->percent = $request->get('percent');
            $promocode->expiration = $request->get('expiration');
            $promocode->save();
            return redirect()->route('promocodes-index');
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code = Promocode::find($id);
        $code->delete();
        return redirect()->route('promocodes-index');

    }

    public function verify($code)
    {
         $code = \filter_var($code,FILTER_SANITIZE_STRING);
         $pcode = Promocode::where('code',$code)
                    //->whereRaw('expiration >= Now()')
                    ->get(['code','percent','expiration','company']);
         if(  !$pcode->isEmpty() ) {
            $pcode = $pcode[0]->toArray();
            $expiration = $pcode['expiration'];
            if ($expiration) {
                if (strtotime($expiration) < time()) {
                    $pcode['error'] = 'Promocode has expired';
                    $pcode['percent'] = 0;
                } else {
                    $pcode['valid'] = true;
                }
            }
        }else{
            $pcode['error'] = 'Promocode does not exist';
        }
         return $pcode;
    }
}
