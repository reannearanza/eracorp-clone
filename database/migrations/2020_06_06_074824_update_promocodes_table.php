<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promocodes', function(Blueprint $table) {
            if (!Schema::hasColumn('promocodes', 'company')) {
                Schema::table('promocodes', function (Blueprint $table) {
                    $table->string('company', 30)->nullable()->after('code');
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promocodes', function(Blueprint $table) {
            $table->dropColumn(['company']);
        });
    }
}
