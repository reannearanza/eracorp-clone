/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);


import VModal from 'vue-js-modal'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)
Vue.use(VueSweetalert2);
Vue.use(VModal)
Vue.use(require('vue-moment'));


import MemberBilling from './components/MemberBilling'
import MemberPersonalInformation from './components/MemberPersonalInformation'
import MemberHealthInformation from './components/MemberHealthInformation'
import MemberRegisteredVehicle from './components/MemberRegisteredVehicle'
import TermsAndConditions from './components/TermsAndConditions'
import PhotoUploader from './components/PhotoUploader'
import EmergencyContacts from './components/EmergencyContacts'
import Checkout from './components/Checkout'
import SystemUsers from './components/SystemUsers'
import ContactUs from './components/ContactUs'
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

$('.custom-modal-btn-close').click(function() {
    $('.overlay').css('display', 'none');
})

const app = new Vue({
    'components' :{
        MemberBilling,
        MemberPersonalInformation,
        MemberHealthInformation,
        MemberRegisteredVehicle,
        TermsAndConditions,
        PhotoUploader,
        EmergencyContacts,
        Checkout,
        SystemUsers,
        ContactUs
    },
    el: '#app',
});
