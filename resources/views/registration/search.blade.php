@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Search Result/s</div>
                                <div class="card-body">

                                <div id="search" class="py-4">
                                        <form action="/admin/registration/search" method="GET">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td style="width:50%">
                                                    <input name="search" class="form-control" type="text" placeholder="Search" aria-label="Search">
                                                    </td>
                                                    <td style="width:30%">
                                                        <select  name="by" id="" class="form-control">
                                                            <option value="email" selected>Email</option>
                                                            <option value="firstname">First name</option>
                                                            <option value="lastname">Last name</option>
                                                        </select>
                                                    </td>
                                                    <td style="width:20%">
                                                        <button class="btn btn-info btn-block">
                                                        <i class="fab fa-searchengin"></i>
                                                            Search
                                                        </button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                    <table class="table table-responsive-sm">
                                    <tr>
                                        <th>ID</th>   
                                        <th>ST Account</th>
                                            <th>First name</th>
                                            <th>Last name</th>
                                            <th>Email</th>
                                            <th>Mobile Number</th>
                                            <th>Action</th>
                                        </tr>
                                    @foreach($records as $record)
                                    <tr>
                                        @foreach($record as $k=>$v)

                                                <td>{{$v}}</td>

                                        
                                        @endforeach
                                        <td>
                                        <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                                       aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="/admin/registration/edit/{{$record['id']}}">
                                                            Edit
                                                        </a>
                                                        @can('isAdmin')
                                                        <a class="dropdown-item" onclick="confirmDelete({{$record['id']}})">
                                                            Delete
                                                        </a>
                                                        @endcan
                                                    </div
                                        </td>
                                    </tr>
                                    @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    @endsection




