<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThankYouForSigningUp extends Mailable
{
    use Queueable, SerializesModels;

    protected $member;
    protected $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $member,$password)
    {
        $this->member = $member;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Thank you for signing up')
            ->view('emails.member-signup-confirmation',['member'=>$this->member,'password'=>$this->password]);
    }
}
