<?php

namespace App\Jobs;

use App\Mail\ThankYouForSigningUp;
use App\Registration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendThankYouEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $registrant;
    protected $password;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Registration $registrant,$password)
    {
        $this->registrant = $registrant;
        $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mail = new ThankYouForSigningUp($this->registrant,$this->password);
        Mail::to($this->registrant->txtEmail)->send($mail);
    }
}
