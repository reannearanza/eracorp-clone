<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registrations', function(Blueprint $table) {
            if (!Schema::hasColumn('registrations', 'oImage')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('oImage', 30)->after('txtMobileNum');
                });
            }
            if (!Schema::hasColumn('registrations', 'txtMobileType')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtMobileType', 30)->after('oImage');
                });
            }
            if (!Schema::hasColumn('registrations', 'txtSpecificCond')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtSpecificCond', 30)->after('txtMobileType');
                });
            }

            if (Schema::hasColumn('registrations', 'txtEmergencyName')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->dropColumn('txtEmergencyName');
                });
            }
            if (Schema::hasColumn('registrations', 'txtEmergencyNumber')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->dropColumn('txtEmergencyNumber');
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registrations', function(Blueprint $table) {
            $table->string('txtEmergencyName', 30);
            $table->string('txtEmergencyNumber', 30);
            $table->dropColumn(['oImage']);
        });
    }
}
