@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">Promo Codes / Create</div>
        <div class="card-body">
            <form action="{{route('promocodes-update',['id'=>$code['id']])}}" method="POST">
                @csrf
                <table class="table table-responsive-sm table-hover table-outline mb-0">
                    <thead class="thead-light">
                    <tr>
                        <th>Code</th>
                        <th>Company</th>
                        <th>%</th>
                        <th>Expiration</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            @csrf
                            <input type="hidden" value="{{$code['id']}}" name="id">
                            <input type="text" value="{{$code['code']}}" class="form-control" name="code" required>
                        </td>
                        <td>
                            <input type="text" value="{{$code['company']}}" class="form-control" name="company" required>
                        </td>
                        <td>
                            <input type="number" value="{{$code['percent']}}" class="form-control" name="percent" required>
                        </td>
                        <td>
                            <input type="date" value="{{ date('Y-m-d',strtotime($code['expiration']))}}" class="form-control" name="expiration" required>
                        </td>
                        <td>
                            <button class="btn btn-info" type="submit">
                                <i class="fa fa-save"></i>
                                Save
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
@endsection