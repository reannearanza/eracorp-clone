<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public static function write($content){
        $self = new self();
        $self->content = $content;
        $self->save();
    }
}
