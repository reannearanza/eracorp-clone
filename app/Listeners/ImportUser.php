<?php

namespace App\Listeners;

use App\Events\ImportUsersEvent;
use App\Jobs\BatchPayment;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use GuzzleHttp\Client;

class ImportUser implements ShouldQueue
{

    public $timeout = 120000;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ImportUsersEvent  $event
     * @return void
     */
    public function handle(ImportUsersEvent $event)
    {

        // save the file
        // dispatch an event to process the jobs

        set_time_limit(0);

        foreach($event->file as $key=>$line){
            // loop over and register each user
            sleep(1);
            if($key > 0){

                $data = explode(',',$line);
                $contacts = explode(';',$data[8]);
                $contact_numbers = explode(';',$data[9]);
                $emergency_contacts = [];

                foreach($contacts as $index=>$contact){
                    $emergency_contacts[$index] = [
                        "sEFullName"=>$contacts[$index],
                        'sEMobileNum'=>$contact_numbers[$index]
                    ];
                }

                $client = new Client();
 
                $member = [
                    'txtEmail'=>$data[0],
                    'txtFirstName'=>$data[1],
                    'txtLastName'=>$data[2],
                    'txtBirthdate'=>$data[3],
                    'txtHomeAddress'=>$data[4],
                    'txtMobileNum'=>$data[5],
                    'aEmergencyContacts'=>json_encode($emergency_contacts),
                    'txtMobileType'=>$data[6],
                    'txtSpecificCond'=>$data[7],
                    "txtCodeWord"=>$data[10],
                ];

                try{

                    // check if email already exist skip and notify admin that the email has already been used!   
                    
                    $result = User::where('email',$data[0])->get()->first();
                    
                    if($result){
                        continue;
                    }


                    $client->request('POST', env('APP_URL').'/api/registration/store', 
                                    ['form_params' => $member]);

                
                    BatchPayment::dispatch($data);
                

                }catch(\Exception $e){
                    dump( $e->getMessage() );
                }
            }
        }
        dump('done');
    }
}
