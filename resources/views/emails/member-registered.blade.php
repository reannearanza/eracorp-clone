Hello Admin,</br>

<p>A new member has signed up.</p>

<p>Here are the member's details:</p>

<table style="border:1px solid #ccc;width:500px;padding:20px;">
    @foreach(  $member as $key=>$val)
    <tr  style="border-bottom: 1px solid #000">
        <td width="40%">{{str_replace('txt','',$key)}}</td>
        <td>{{$val}}</td>
    </tr>
    @endforeach
</table>

