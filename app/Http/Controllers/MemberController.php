<?php

namespace App\Http\Controllers;

use App\Registration;
use App\EmergencyContact;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->isMember();
        return view('member.index');
    }
    public function getOrder($id){
        $orders = Order::where('user_id',$id)->get();
        return $orders;
    }

    public function getPersonalInfo($id){
        $user = User::with('profile')->find($id);
        if (!empty($user->profile->oImage)) {
            $user->profile->oImage = asset('storage/uploads/' . $user->profile->oImage);
        }
        return $user->profile;
    }

    public function profile()
    {
        $this->isMember();
        $user = User::find(auth()->user()->id);
    
        
        // $profile = Registration::where('id',auth()->user()->profile_id);
        // $profile = $profile->get();
        // if($profile == null){
        //     $data = [];
        // }else{
        //     $data = $profile->first()->toArray();
        // }
        // $image = !empty($data['oImage']) ? asset('/storage/uploads/'.$data['oImage']) : "";
        return view('member.profile',['user'=>$user]);
    }

    public function emergencyContacts()
    {
        $emergencyContacts = EmergencyContact::where('id', auth()->user()->profile_id);
        $emergencyContacts = $emergencyContacts->get()->toArray();

        return view('member.contacts', ['userId' => auth()->user()->profile_id, 'contacts' => $emergencyContacts]);
    }

    public function saveProfile(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'txtFirstName'   => 'required',
            'txtLastName'    => 'required',
            'txtBirthdate'   => 'required',
            'txtMobileNum'   => 'required',
            'txtHomeAddress' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['Success' => false, 'Errors' => $validator->errors()->all()]);
        }

        $res = Registration::where('id', $id)->update([
            'txtFirstName'   => $request->txtFirstName,
            'txtLastName'    => $request->txtLastName,
            'txtBirthDate'   => $request->txtBirthdate,
            'txtMobileNum'   => $request->txtMobileNum,
            'txtHomeAddress' => $request->txtHomeAddress
        ]);

        return response()->json(['Success' => $res]);
    }

    public function saveHealthInfo(Request $request, $id){
        $validator = Validator::make($request->all(), [
           // 'txtMedical'             => 'required',
           // 'txtHealthCareProvider'  => 'required',
           // 'txtHealthPlan'          => 'required',
           // 'txtPolicyNumber'        => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['Success' => false, 'Errors' => $validator->errors()->all()]);
        }

        $res = Registration::where('id', $id)->update([
            'txtMedical'             => $request->txtMedical,
            'txtHealthCareProvider'  => $request->txtHealthCareProvider,
            'txtHealthPlan'          => $request->txtHealthPlan,
            'txtPolicyNumber'        => $request->txtPolicyNumber,
        ]);

        return response()->json(['Success' => $res]);
    }

    public function saveRegisteredVehicle(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'txtCarColor'    => 'required',
            'txtCarMake'     => 'required',
            'txtYearModel'   => 'required',
            'txtPlateNumber' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['Success' => false, 'Errors' => $validator->errors()->all()]);
        }

        $res = Registration::where('id', $id)->update([
            'txtCarColor'    => $request->txtCarColor,
            'txtCarMake'     => $request->txtCarMake,
            'txtYearModel'   => $request->txtYearModel,
            'txtPlateNumber' => $request->txtPlateNumber,
        ]);

        return response()->json(['Success' => $res]);
    }

    public function saveContacts(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'aEmergencyContacts' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['Success' => false, 'Errors' => $validator->errors()->all()]);
        }

        EmergencyContact::where('id',$id)->delete();

        foreach($request->aEmergencyContacts as $index => $contacts) {
            $emergencyContact = new EmergencyContact();
            $emergencyContact->id = $id;
            $emergencyContact->sEFullname = $contacts['sEFullName'];
            $emergencyContact->sEMobileNum = $contacts['sEMobileNum'];
            $emergencyContact->save();
        }

        return $request->aEmergencyContacts;
    }

    public function saveImage(Request $request, $id)
    {
        $oImage = $request->file('oImage');
        $newFileName = uniqid() . '.' . $oImage->getClientOriginalExtension();
        $oImage->move(storage_path('/app/public/uploads'), $newFileName);

        $res = Registration::where('id', $id)
          ->update(['oImage' => $newFileName]);

        return response()->json(['Success' => $res, 'image' => asset('storage/uploads/' . $newFileName)]);
    }

    public function billing()
    {
        $this->isMember();
        $user = User::with('profile')->find(auth()->user()->id);
        return view('member.billing',['user'=>$user]);
    }

    public function terms()
    {
        $this->isMember();
        $user = User::find(auth()->user()->id);
        return view('member.terms', ['user' => $user]);
    }
    
    private function isMember()
    {
        if (!\Gate::allows('isMember')) {
            abort(403, "Invalid user permission!");
        }
    }
}
