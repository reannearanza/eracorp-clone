<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('reference_id');
            $table->integer('user_id');
            $table->integer('amount_paid'); // paynamicx throws error on float values e.g  1.35 cents
            $table->integer('discount_percent');
            $table->string('promocode');
            $table->string('status'); // [pending,processing,done]
            $table->string('email'); // registrant's email
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
