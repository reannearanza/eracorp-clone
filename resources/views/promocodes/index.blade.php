@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">Promo Codes</div>
        <div class="card-body">

            <div class="pb-3">
                <a href="{{route('promocodes-create')}}" class="btn btn-danger">
                    <i class="fas fa-plus-circle"></i>
                    Add Code
                </a>
            </div>

            <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>Code</th>
                    <th>Company</th>
                    <th>%</th>
                    <th>Expiration</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($records as $code)
                    <tr>
                        <td>{{$code->code}}</td>
                        <td>{{$code->company}}</td>
                        <td>{{$code->percent}}</td>
                        <td>{{$code->expiration}}</td>
                        <td class="text-center">
                            <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-cog"></i>
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item"
                                   href="{{route('promocodes-edit',['id'=>$code->id])}}">
                                    Edit
                                </a>
                                <a class="dropdown-item"
                                   href="{{route('promocodes-delete',['id'=>$code->id])}}">
                                    Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="my-lg-4">
                {{$records->links()}}
            </div>
        </div>
    </div>

@endsection