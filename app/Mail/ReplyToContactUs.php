<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReplyToContactUs extends Mailable
{
    use Queueable, SerializesModels;
    protected $email;
    protected $name;
    protected $message;
    protected $reply;

    /**
     * Create a new message instance.
     * @param $email
     * @param $name
     * @param $message
     * @param $reply
     * 
     * @return void
     */
    
    public function __construct($email,$name,$message,$reply)
    {
        $this->email = $email;
        $this->name = $name;
        $this->message = $message;
        $this->reply = $reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact-us-reply',[
                'email'=>$this->email,
                'name'=>$this->name,
                '_message'=>$this->message,
                'reply'=>$this->reply
            ]);
    }
}
