<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;
use App\Http\Resources\ContactUsResource;
use App\Mail\ReplyToContactUs;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
 {
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
 {
        //$contactUsCollection = ContactUs::paginate( 15 );
        $contactUsCollection = ContactUs::orderBy('created_at','desc')->get();
        return ContactUsResource::collection( $contactUsCollection );
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */

    public function store( Request $request )
 {
        $request->validate( [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ] );

        $contactUs = new ContactUs;
        $contactUs->name = $request->input( 'name' );
        $contactUs->email = $request->input( 'email' );
        $contactUs->message = $request->input( 'message' );

        if ( $contactUs->save() ) {
            return new ContactUsResource( $contactUs );
        }
    }

    /**
    * Display the specified resource.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */

    public function show( $id )
 {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param \Illuminate\Http\Request $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */

    public function update( Request $request, $id )
 {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */

    public function destroy( $id )
 {
        //
    }

    /**
    *  @param \Illuminate\Http\Request $request
    */

    public function reply( Request $request ) {
        try{
            $email = $request->get('email');
            $replyEmail = new ReplyToContactUs($email,$request->input('name'),$request->input('message'),$request->input('reply'));
            Mail::to($email)->send($replyEmail);
            $contactus = ContactUs::find($request->get('id'));
            if($contactus){
                $contactus->replied = "Yes";
                $contactus->save();
            }
            return ['success'=>true,'contactusList'=>ContactUs::orderBy('created_at','desc')->get()];
        }catch(\Exception $e){
            return response($e->getMessage(),500);
        }

        
    }
}
