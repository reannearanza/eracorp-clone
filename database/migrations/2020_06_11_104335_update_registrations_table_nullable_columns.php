<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRegistrationsTableNullableColumns extends Migration
{
        /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registrations', function(Blueprint $table) {
            if (Schema::hasColumn('registrations', 'oImage')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('oImage',30)->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtMedical')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtMedical',30)->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtHealthCareProvider')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtHealthCareProvider',30)->nullable()->change();
                });
            }
            
            if (Schema::hasColumn('registrations', 'txtPolicyNumber')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->integer('txtPolicyNumber')->unsigned()->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtHealthPlan')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtHealthPlan',30)->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtCarColor')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtCarColor',30)->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtCarMake')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtCarMake',30)->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtYearModel')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtYearModel',30)->nullable()->change();
                });
            }

            if (Schema::hasColumn('registrations', 'txtPlateNumber')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('txtPlateNumber',30)->nullable()->change();
                });
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registrations', function(Blueprint $table) {
            $table->string('txtMedical',30)->change();
            $table->string('txtHealthCareProvider',30)->change();
            $table->integer('txtPolicyNumber')->change();
            $table->string('txtHealthPlan',30)->change();
            $table->string('txtCarColor',30)->change();
            $table->string('txtCarMake',30)->change();
            $table->string('txtYearModel',30)->change();
            $table->string('txtPlateNumber',30)->change();
        });
    }
}
