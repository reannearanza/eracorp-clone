@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Payments - Edit</div>
                                <div class="card-body">
                                    <form action="/admin/payments/update/{{$payment->id}}" method="POST">
                                        @csrf
                                        <table class="table">
                                            <tr>
                                                <td width="20%">
                                                    ID
                                                </td>
                                                <td>
                                                    {{$payment->id}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    User ID
                                                </td>
                                                <td>
                                                    {{$payment->user_id}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email
                                                </td>
                                                <td>
                                                    {{$payment->email}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Amount Paid
                                                </td>
                                                <td>
                                                    {{$payment->amount_paid}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Discount Code
                                                </td>
                                                <td>
                                                    {{$payment->discount_code}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Company
                                                </td>
                                                <td>
                                                    {{$payment->company}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Didcount %
                                                </td>
                                                <td>
                                                    {{$payment->discount_percent}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Payment Method
                                                </td>
                                                <td>
                                                    {{$payment->method}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Payment Status
                                                </td>
                                                <td>
                                                    <select name="payment-status" class="form-control">
                                                        <option selected value="{{$payment->status}}">{{$payment->status}}</option>
                                                        @if($payment->status != 'Payment success')
                                                            <option value="Payment success">Payment success</option>
                                                        @endif
                                                        @if($payment->status != 'Payment failed')
                                                            <option value="Payment failed">Payment failed</option>
                                                        @endif
                                                        @if($payment->status != 'Pending')
                                                            <option value="Pending">Pending</option>
                                                        @endif
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <button class="btn btn-danger">
                                                        <i class="fa fa-save"> </i>
                                                        Save
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- /.row-->
                </div>
            </div>
        </main>

    </div>
@endsection