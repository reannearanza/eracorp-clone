Hello,{{ucwords($user)}}<br>

<p>
    Congratulations! You are now subscribed to ERA! Enjoy your one year free subscription and live life at ease with less to worry about.
</p>
<p>
    Please allow our team to process your registration, we will send you the instructions to download and activate your account to our command center within a 24 hour period.
</p>
<p>
    Thank you.
</p>
<p>
    Customer Support Team<br>
    Emergency Rapid Assistance
</p>



