<?php

namespace App\Http\Controllers;

use App\Promocode;
use App\Registration;
use App\User;
use Illuminate\Http\Request;
use App\Mail\memberHasRegistered;
use  Illuminate\Support\Facades\Mail;
class TestController extends Controller
{
    public function mail(){
        try{
            $member = Registration::first();
            //$mail = new memberHasRegistered($member);
            Mail::to('jaranza@gmail.com')->later(5,new memberHasRegistered($member));
            return ['done'=>true];
        }catch(\Exception $e){
            print_r($e->getMessage());
        }

    }
    public function mailtemplate(){
        $members = Registration::all();
        $member  = $members->find(1);
//        echo '<pre>';
//        print_r(json_encode($member));
//        echo '</pre>';
        return view('emails.member-registered',['member'=>$member->toArray()]);
    }

    public function test(){
      echo "<pre>";
      $payments = \App\Payment::all();
      foreach($payments as $payment){


              $user = \App\User::where('email',$payment->email)->first(['id']);

              if(isset($user->id)) {
                  $payment->user_id = $user->id;
                  $payment->save();
              }

      }


    }
    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    // {"paymentresponse":"PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U2VydmljZVJlc3BvbnNlV1BGIHhtbG5zOnhzZD0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiPjxhcHBsaWNhdGlvbj48bWVyY2hhbnRpZD4wMDAwMDAwODA1MjBCNzFBQjFFNDwvbWVyY2hhbnRpZD48cmVxdWVzdF9pZD4xNTMwODcwOTkxNWVkZjZjMGE3MWE5ZTguNDc2NjE1Nzk8L3JlcXVlc3RfaWQ PHJlc3BvbnNlX2lkPjIwODkwMDkwMjQ2NDExMjg2NjE1PC9yZXNwb25zZV9pZD48dGltZXN0YW1wPjIwMjAtMDYtMDlUMTk6MDY6MTkuMDAwMDAwKzA4OjAwPC90aW1lc3RhbXA PHJlYmlsbF9pZCAvPjxzaWduYXR1cmU YjA4Y2ZlZDEzZDJmYzk5NWQwOWU4MDkyZTM2NjNkZWIxNjQ2NDg4NTU1Yzc5NmZjNTIzMjRjMDIwNjdlNjgzMTg5N2MzOWI0N2MzODU1YTY1NTEzNjg0NDk1NjUzMDA3ZDU5ZTQ4NjBiNDdiNjc5Y2RmZDM1NGQ1YTcyYjE3NTQ8L3NpZ25hdHVyZT48cHR5cGU QlBJT05MSU5FPC9wdHlwZT48L2FwcGxpY2F0aW9uPjxyZXNwb25zZVN0YXR1cz48cmVzcG9uc2VfY29kZT5HUjAwMzwvcmVzcG9uc2VfY29kZT48cmVzcG9uc2VfbWVzc2FnZT5UcmFuc2FjdGlvbiBGYWlsZWQ8L3Jlc3BvbnNlX21lc3NhZ2U PHJlc3BvbnNlX2FkdmlzZT5UcmFuc2FjdGlvbiBoYXMgZmFpbGVkLjwvcmVzcG9uc2VfYWR2aXNlPjxwcm9jZXNzb3JfcmVzcG9uc2VfaWQ UFRYOTMwMDgzOTE1NjI2MjwvcHJvY2Vzc29yX3Jlc3BvbnNlX2lkPjxwcm9jZXNzb3JfcmVzcG9uc2VfYXV0aGNvZGUgLz48L3Jlc3BvbnNlU3RhdHVzPjxzdWJfZGF0YSAvPjx0cmFuc2FjdGlvbkhpc3Rvcnk PHRyYW5zYWN0aW9uIC8 PC90cmFuc2FjdGlvbkhpc3Rvcnk PE1ldGFEYXRhIC8 PC9TZXJ2aWNlUmVzcG9uc2VXUEY","paymentResponseCode":"<response_code>GR003<\/response_code>"}
}
