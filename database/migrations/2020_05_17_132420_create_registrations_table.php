<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->id();
            $table->string('txtEmail',40);
            $table->string('txtPromoCode',30);
            $table->string('txtFirstName',30);
            $table->string('txtLastName',30);
            $table->string('txtBirthdate',30);
            $table->string('txtHomeAddress',30);
            $table->string('txtMobileNum',30);
            $table->string('txtMedical',30);
            $table->string('txtEmergencyName',30);
            $table->string('txtEmergencyNumber');
            $table->string('txtCodeWord',30);
            $table->string('txtHealthCareProvider',30);
            $table->integer('txtPolicyNumber');
            $table->string('txtHealthPlan',30);
            $table->string('txtCarColor',30);
            $table->string('txtCarMake',30);
            $table->string('txtYearModel',30);
            $table->string('txtPlateNumber',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
