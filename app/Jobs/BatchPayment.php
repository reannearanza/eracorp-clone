<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;
use App\User;
use App\Payment;

class BatchPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $paymentData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($paymentData)
    {
        $this->paymentData = $paymentData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::where('email',$this->paymentData[0])->get()->first();
        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->email = $this->paymentData[0];
        $payment->amount_paid = 0;
        $payment->discount_code = $this->paymentData[11];
        $payment->discount_percent = 100;
        $payment->method = "via-company";
        $payment->company = $this->paymentData[12]; 
        $payment->status = 'pemding';
        $payment->save();
    }
}
