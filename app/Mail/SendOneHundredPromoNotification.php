<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOneHundredPromoNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $clientEmail;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($clientEmail,$user)
    {
        $this->clientEmail = $clientEmail;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->clientEmail)
                ->bcc('gado.aranza@gmail.com')
                ->cc(env('ONEHUNDRED_PROMO_TO','era.payment@hdt.ph'))
                ->from(env('ONEHUNDRED_PROMO_FROM','era.registration@hdt.ph'))
                ->subject(env('ONEHUNDRED_PROMO','Era Free Subscription'))
                ->view('emails.onehundred-percent-notif',['user'=>$this->user]);
    }
}
