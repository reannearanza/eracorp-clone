<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = [
        'txtEmail',
        'txtFirstName',
        'txtLastName',
        'txtBirthdate',
        'txtHomeAddress',
        'txtMobileNum',
        'txtMedical',
        'aEmergencyContacts',
        'txtHealthCareProvider',
        'txtCodeWord',
        'txtPolicyNumber',
        'txtHealthPlan',
        'txtCarColor',
        'txtCarMake',
        'txtYearModel',
        'txtPlateNumber',
        'oImage',
        'processed'
    ];
}

