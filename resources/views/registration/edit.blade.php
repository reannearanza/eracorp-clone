@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">Registrations</div>
                                <div class="card-body">
                                    <form action="{{route('registration-update',['id'=>$user['id']])}}" method="post">
                                        @csrf
                                        <table class="table">
                                            @foreach($user as $key=>$value)
                                                @if(!in_array($key,
                                                                ['id',
                                                                'txtCarColor',
                                                                'txtPolicyNumber',
                                                                'txtCarMake',
                                                                'txtYearModel',
                                                                'txtPlateNumber'
                                                                ]
                                                                ))
                                                    <tr>
                                                        <td>{{ucfirst(str_replace('txt','',$key))}}</td>
                                                        @if($key=='processed')
                                                            <td>
                                                                <select class="form-control" name="{{$key}}">
                                                                    <option value="{{$value}}"
                                                                            selected>{{$value}}</option>
                                                                    <option value="Yes">Yes</option>
                                                                    <option value="No">No</option>

                                                                </select>
                                                            </td>
                                                        @else
                                                            <td>
                                                                @if(in_array($key,['created_at','updated_at']))
                                                                    {{ Carbon\Carbon::parse($value)->format('m/d/Y') }}
                                                                @elseif((in_array($key,['txtEmail'])))
                                                                    {{$value}}
                                                                @else
                                                                    <input class="form-control" type="text"
                                                                           value="{{$value}}" name="{{$key}}">
                                                                @endif
                                                            </td>
                                                        @endif

                                                    </tr>
                                                @endif
                                            @endforeach

                                            <tr>
                                                <td colspan="2">
                                                    <strong>Emergency Contacts</strong>
                                                </td>
                                            </tr>

                                            @foreach($contacts as $key=>$contact)
                                                <tr>
                                                    <td class="pl-4"> Contact Name</td>
                                                    <td> {{$contact->sEFullName}}</td>
                                                </tr>
                                                    <tr>
                                                        <td class="pl-4"> Contact Number</td>
                                                        <td> {{$contact->sEMobileNum}}</td>
                                                    </tr>
                                                @endforeach

                                            <tr>
                                                <td colspan="2" class="text-right">

                                                    <a href="/admin" class="btn btn-info">
                                                        <i class="fas fa-users"> </i>
                                                        Back To List
                                                    </a>
                                                    <button class="btn btn-info" type="submit">
                                                        <i class="fa fa-save pr-2"></i>
                                                        Save
                                                    </button>
                                            </tr>
                                        </table>
                                    </form>

                                    @endsection
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>




