<?php

namespace App\Http\Controllers;

use App\User;
use App\Util;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');

    }
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Util::isAdmin();
        $users = User::whereIn('user_type',['admin','operator'])->get();
        return view('users.index',['users'=>$users]);
    }

    // public function getUsers(){
    //     return User::paginate(5);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Util::isAdmin();

        if($request->has('id')){
            $user = User::find( $request->get('id') );
            $user->user_type = $request->get('user_type');
            $user->name = $request->get('name');
            $user->save();
            return $user->only(['email','user_type']);
        }else{
            $user =  new User;
            $user->user_type  = $request->get('user_type');
            $user->name  = $request->get('name');
            $user->email = $request->get('email');
            $user->profile_id = ""; 
            $user->password = bcrypt($request->get('password','pass'.time()));
            $user->save();
            return User::whereIn('user_type',['admin','operator'])->get();
        }
        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Util::isAdmin();

        $user = User::find($id);
        if($user){
            $user->delete();
            return User::whereIn('user_type',['admin','operator'])->get();
        }
    }
}
