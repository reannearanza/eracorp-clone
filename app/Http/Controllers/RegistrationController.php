<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\Http\Resources\ContactUsResource;
use App\Jobs\CreateUser;
use App\Jobs\SendThankYouEmail;
use App\Log;
use App\Mail\memberHasRegistered;
use App\Mail\ThankYouForSigningUp;
use App\Registration;
use App\EmergencyContact;
use App\Http\Resources\RegistrationResource;
use App\User;
use App\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$registrationCollection = Registration::paginate(15);
        $registrationCollection = DB::table('registrations')->orderBy('created_at','desc')->paginate(100);
        return RegistrationResource::collection($registrationCollection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$oImage = $request->file('oImage');

        //$newFileName = uniqid() . '.' . $oImage->getClientOriginalExtension();

        //$oImage->move(storage_path('/app/public/uploads'), $newFileName);
        
        $validator = Validator::make($request->all(), [
            'txtEmail' => 'required|email',
            'txtFirstName' => 'required',
            'txtLastName' => 'required',
            'txtHomeAddress' => 'required',
            'txtMobileType' => 'required',
            'txtSpecificCond' => 'required',
            'aEmergencyContacts' => 'required',
            'txtHealthCareProvider' => 'required',
            'txtCodeWord' => 'required',
           // 'txtMedical' => 'required',
            //'txtPolicyNumber' => 'required',
            //'txtHealthPlan' => 'required',
           // 'txtCarColor' => 'required',
           // 'txtCarMake' => 'required',
           // 'txtYearModel' => 'required',
           // 'txtPlateNumber' => 'required',
           // 'oImage' => 'required|image'
        ]);

        $data = $request;
        $registration = new Registration;
        $registration->txtEmail = $data->txtEmail;
        //$registration->txtPromoCode = $data->txtCodeWord;
        $registration->txtFirstName = $data->txtFirstName;
        $registration->txtLastName  = $data->txtLastName;
        $registration->txtBirthdate = $data->txtBirthdate;
        $registration->txtHomeAddress = $data->txtHomeAddress;
        $registration->txtMobileNum = $data->txtMobileNum;
        $registration->txtMedical = isset($data->txtMedical) ? $data->txtMedical : ""; // fix fe
        $registration->txtMobileType = $data->txtMobileType;
        $registration->txtSpecificCond = $data->txtSpecificCond;
        $registration->txtCodeWord = $data->txtCodeWord;
        $registration->save();

        $aEmergencyContacts = json_decode($data->aEmergencyContacts, true);

        foreach($aEmergencyContacts as $index => $contacts) {
            $emergencyContact = new EmergencyContact();
            $emergencyContact->id = $registration->id;
            $emergencyContact->sEFullname = $contacts['sEFullName'];
            $emergencyContact->sEMobileNum = $contacts['sEMobileNum'];
            $emergencyContact->save();
        }
        
        if ($registration->save()) {
            $password = Util::randomPassword();
            CreateUser::dispatch($registration,$password);
            SendThankYouEmail::dispatch($registration,$password);
            return new RegistrationResource($registration);
        }
    }

    public function email(Request $request){
        $reg = new Registration;
        $user = $reg->where('txtEmail', $request->email)->get();
        return $user->count() > 0 ? true : false; 
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $params = $request->except(['_token','id']);
        //dd($params);
        $user = Registration::where('id',$id);
        $user = $user->first();
        
        $user->st_account = $params['st_account'];
        $user->processed = $params['processed'];
        //$user->txtEmail = $params['txtEmail'];
        $user->txtPromoCode = $params['txtPromoCode'];                  
        $user->txtFirstName = $params['txtFirstName'];
        $user->txtLastName = $params['txtLastName'];
        $user->txtBirthdate = $params['txtBirthdate'];
        $user->txtHomeAddress = $params['txtHomeAddress'];
        $user->txtMobileNum = $params['txtMobileNum'];
        $user->oImage = $params['oImage'];
        $user->txtMobileType = $params['txtMobileType'];
        $user->txtSpecificCond = $params['txtSpecificCond'];
        $user->txtMedical = $params['txtMedical'];
        $user->txtCodeWord = $params['txtCodeWord'];
        $user->txtHealthCareProvider = $params['txtHealthCareProvider'];
        $user->txtPolicyNumber = @$params['txtPolicyNumber'];
        $user->txtHealthPlan = @$params['txtHealthPlan'];
        $user->txtCarColor = @$params['txtCarColor'];
        $user->txtCarMake = @$params['txtCarMake'];
        $user->txtYearModel = @$params['txtYearModel'];
        $user->txtPlateNumber = @$params['txtPlateNumber'];

        $user->save();
        //print_r($user);exit;
        return redirect()->route('registration-edit',['id'=>$id]);
    }

    public function edit(Request $request, $id)
    {
        $record = Registration::find($id);
        $contacts = EmergencyContact::where('id',$id)->get();
        $user = $record->toArray();
        return view('registration.edit',['user'=>$user,'contacts'=>$contacts]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Registration::find($id);
        if($user)
            $user->delete();
        return redirect()->route('admin-index',['id'=>$user->id]);
    }

    public function search(Request $request){
        switch($request->get('by')){
            case 'lastname':
                $where = 'txtLastName';
                break;
            case 'firstname':
                $where = 'txtFirstName';
            break;
            case 'email':
            default:
                    $where = 'txtEmail';
        }
        $search = $request->get('search');
        $result = Registration
                            //where($where,$search)
                            ::Orwhere($where,'like',"%$search%")
                            ->get([
            'id',
            'st_account',
            'txtFirstName',
            'txtLastName',
            'txtEmail',
            'txtMobileNum'
        ]);
        if( $result->count() ){
            $records = $result->toArray();
        }else{
            $records = [];
        }
        return view('registration.search',['records'=>$records]);
    }
}
