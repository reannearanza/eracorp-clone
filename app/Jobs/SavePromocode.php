<?php

namespace App\Jobs;

use App\Log;
use App\Mail\SendOneHundredPromoNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Promocode;
use Illuminate\Support\Facades\Mail;

class SavePromocode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $promocode;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email,$promocode)
    {
        $this->email = $email;
        $this->promocode = $promocode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $user = User::where('email',$this->email)->get();
            $promocode = Promocode::where('code',$this->promocode)->get();
            $user->first()->promocodes()->save($promocode->first());

            // let's send a notification , era.registration@hdt.ph as the sender to era.payment@hdt.ph
            Mail::send(new SendOneHundredPromoNotification($this->email,$user->first()->name));


        }catch(\Exception $e){
            Log::write($e->getMessage());
        }

    }
}
