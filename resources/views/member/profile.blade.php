@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-6">
                            <member-personal-information :user="{{$user}}"></member-personal-information>
                        </div>
                        <div class="col-md-6">
                            <member-health-information :user="{{$user}}"></member-health-information>
                            <member-registered-vehicle :user="{{$user}}"></member-registered-vehicle>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="{{ asset('js/member/profile.js') }}" type="module"></script>
@endsection
