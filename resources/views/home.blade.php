@extends('layouts.app')
@section('content')
<div id="custom-modal-background" class="overlay">
    <div class="custom-modal">
    <a href="/#custom-modal-background" class="custom-modal-btn-close">&times;</a>
    <img src="img/img-ERA-Nmax-promo.jpg"/>
    </div>
</div>

<a id="home" name="home"></a>
<div class="heading">
    <div class="container py-sm-5 pt-3">
        <?php /*?><img src="img/img-text-help-is-within-reach01.png" class="img-fluid text-help" alt="Help is within reach"><?php */?>
        <img src="img/img-hand-mobile-sos02.gif" class="img-fluid hand-cp d-lg-none d-block" alt="SOS">
    </div>
</div>

<div class="price">
    <div class="container py-md-5 py-4">
        <div class="row justify-content-end">
            <div class="col-lg-7">
                <div class="pl-xl-5 pl-0 ml-xl-3 ml-0">
                    <div class="row py-xl-5 py-lg-3 py-0 justify-content-center">
                        <div class="col-lg-12 col-md-7 py-xl-4 py-0">
                            <div class="card my-lg-3 pl-0 pl-lg-3">
                                <div class="card-body">
                                    <p class="text-for-only mb-0">FOR ONLY</p>
                                    <p class="text-amount mb-0"><sup>Php</sup> <span>1,200</span> <small>/ Year</small></p>
                                    <a href="/registration"><img src="img/img-tap-to-register01.png" class="img-fluid" alt="tap to register"></a>
                                </div>
                            </div>
                        </div>
                        <?php /*?>
                        <div class="col-lg-12 col-md-6">
                            <div class="row no-gutters">
                                <div class="col-3">
                                    <img src="img/img-logo-motolite01.png" class="img-fluid m-1" alt="motolite">
                                </div>
                                <div class="col-3">
                                    <img src="img/img-logo-goodyear01.png" class="img-fluid m-1" alt="goodyear">
                                </div>
                                <div class="col-3">
                                    <img src="img/img-logo-aia01.png" class="img-fluid m-1" alt="aia">
                                </div>
                                <div class="col-3 d-sm-block d-none">

                                </div>
                                <div class="col-3">
                                    <img src="img/img-logo-outlast01.png" class="img-fluid m-1" alt="outlast">
                                </div>
                                <div class="col-3">
                                    <img src="img/img-logo-rapide01.png" class="img-fluid m-1" alt="rapide">
                                </div>
                                <div class="col-3">
                                    <img src="img/img-logo-bridgestone01.png" class="img-fluid m-1" alt="bridgestone">
                                </div>
                                <div class="col-3">
                                    <img src="img/img-logo-talk-to-us01.png" class="img-fluid m-1" alt="talk to us">
                                </div>
                            </div>
                        </div>
                        <?php */?>
                    </div>
                </div>
            </div>
        </div>
        <img src="img/img-hand-mobile-sos02.gif" class="img-fluid hand-cp d-lg-block d-none" alt="SOS">
    </div>
</div>

<a id="chairmanmessage" name="chairmanmessage"></a>
<div class="ceo-message">
    <div class="color-overlay">
        <div class="container py-5">
            <div class="row mb-5">
                <div class="col-12">
                    <div class="card text-white py-2">
                        <div class="card-body p-5">
                            <p><em>"ERA BLAZES NEW TRAIL IN EMERGENCY RAPID ASSISTANCE</em></p>
                            <p><em>In times of emergencies, time is of the essence. In many instances it spells the difference between life and death, safety and risk exposure, success and failure.</em></p>
                            <p><em>WHO and HOW to call for assistance become an immediate and critical decision! That is the ERA ADVANTAGE. At the push of a virtual RED PANIC BUTTON in your mobile phone, an operator will be on hand to arrange assistance from first responders and  to notify your loved ones.</em></p>
                            <p><em>ERA PROMISE: YOU ARE NEVER ALONE WHENEVER AND WHEREVER YOU ARE."</em></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center text-white">
                <div class="col-sm-3 order-sm-1">
                    <img src="img/img-chairman01.png" class="d-block mx-auto img-fluid" alt="ERA Chairman">
                </div>
                <div class="col-sm-8 order-sm-2 text-sm-left text-center">
                    <h4><em>John Raña</em></h4>
                    <p><em>ERA Chairman</em></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="video-ad">
    <video width="1920" height="1080" controls class="w-100 h-auto">
        <source src="video/era.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video>
</div>

<a id="about-us" name="about-us"></a>
<div class="about">
    <div class="container py-md-5">
        <h2 class="pt-5 pb-3"><span>ABOUT</span> US</h2>
        <p><strong>OUR BUSINESS MODEL</strong></p>
        <p>We envision ERA to be the leading Philippine company providing technology-driven security services to Filipinos across all walks of life. We made available ERA services in a manner that will suit the needs and the budget of our Filipino customers.</p>
        <p>We are committed to provide fast and reliable emergency assistance anywhere in the country, 24/7. ERA (Emergency Rapid Assistance) powered by HDT is an app that tracks people in case of emergency when a virtual red button is pressed. A team of highly trained individuals will then respond to the user after determining their location at a predefined time.</p>
        <p class="pb-3">Our primary focus is to develop a long-term relationship with our clients. This dedication to long term partnership will allow us to help many more individuals and organizations understand that ERA subscription is more than just an app, but a necessity.</p>
        <p><strong>HISTORY</strong></p>
        <p>ERA – first of its kind in the Philippines. Founders of HDT- Home Defense Technology the mother company of ERA under the leadership of Barnard Eliot President of ERA  and Director Steve Kinzer, HDT's objective is to provide electronic security alarm system to households and businesses with 24/7 response team. They changed the game of the security system industry.</p>
        <p class="pb-3">Fast forward to a year later, with tremendous amount of research, data analytics and deliberation, the team once again, bring forth another breakthrough that would help public safety and security through an app.</p>
        <p><strong>TARGET MARKET</strong></p>
        <p class="pb-3">We aim to gain the trust of these core groups: professionals, business owners, households and anyone who would like to get better assurance that when they are in need of help, someone is there to assist them quickly.</p>
        <p><strong>THE BRILLIANT MINDS BEHIND ERA</strong></p>
        <p class="pb-5">The team who built the company collectively shared their expertise and exceptional knowledge in security, technology and entrepreneurship. Their success in establishing HDT, the mother company of ERA, built their credibility furthermore. </p>
        <div class="row align-items-center mb-5">
            <div class="col-md-6 order-md-2">
                <div class="pl-md-5 pl-0">
                    <h2 class="mb-4"><span>OUR</span> VISION</h2>
                    <p class="mb-md-5 mb-3 pb-md-5 pb-0">To be the leading Philippine company providing technology-driven security services to Filipinos across all walks of life. Our sincere desire in making a difference in the community is our greatest motivation in presenting something helpful and unique at the same time. </p>
                </div>
            </div>
            <div class="col-md-6 order-md-1">
                <div class="pr-md-5 pr-0">
                    <img src="img/img-our-vision02.jpg" class="d-block mx-auto img-fluid" alt="our vision">
                </div>
            </div>
        </div>
        <div class="row align-items-center mb-5">
            <div class="col-md-6">
                <div class="pr-md-5 pr-0">
                    <h2 class="mb-4"><span>OUR</span> APPROACH</h2>
                    <p class="mb-md-5 mb-3 pb-md-5 pb-0">Help within reach.<br>
                    We aim to make ERA a necessity more than just an app. The affordability of the service is our way of sending a message across that everyone from all walks of life deserves help. </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pl-md-5 pl-0">
                    <img src="img/img-our-approach01.jpg" class="d-block mx-auto img-fluid" alt="our approach">
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
                <div class="pl-md-5 pl-0">
                    <h2 class="mb-4"><span>OUR</span> PROCESS</h2>
                    <p class="mb-md-5 mb-3 pb-md-5 pb-0">With the use of a smart phone application, subscribers will be connected to the appropriate responders in cases of emergencies such as crime related, health related, fire, towing service and roadside assistance. </p>
                </div>
            </div>
            <div class="col-md-6 order-md-1">
                <div class="pr-md-5 pr-0">
                    <img src="img/img-our-process01.jpg" class="d-block mx-auto img-fluid" alt="our process">
                </div>
            </div>
        </div>
    </div>
</div>

<?php /*?><div class="sos-button-action">
    <div class="container">
        <a href="#"><img src="img/img-sos02.gif" class="d-block mx-auto img-fluid" alt="SOS"></a>
    </div>
</div>
<div class="sos-button-info">
    <div class="container py-5">
        <h2 class="pt-4 mb-md-5 mb-2"><span>SOS</span> BUTTON</h2>
        <div class="row pb-4">
            <div class="col-md-8 order-md-2">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue tortor eu varius. adipiscing elit. In congue tortor eu varius. In congue tortor eu varius. adipiscing elit. In congue tortor eu varius.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In congue tortor eu varius. adipiscing elit. In congue tortor eu varius. In congue tortor eu varius. adipiscing elit. In congue tortor eu varius.</p>
            </div>
            <div class="col-md-4 order-md-1">
                <img src="img/img-heart-in-pain01.jpg" class="d-block mx-auto img-fluid" alt="SOS Button">
            </div>
        </div>
    </div>
</div><?php */?>

<a id="how-it-works" name="how-it-works"></a>
<div class="how-it-works">
    <div class="container pb-md-5">
        <h2 class="pt-5"><span>HOW</span> IT WORKS</h2>
        <p class="mb-5">By tapping on the red SOS button, the app sends a panic alarm to our command center manned by highly trained individuals. Command center will track the location of the subscriber at a predefined time and will call them in less than a minute to confirm emergency. Once validated, command center will reach out to the appropriate responders for assistance. </p>
    </div>
    <div class="container timeline mt-5">
        <div class="row align-items-center pb-5 mb-5">
            <div class="col-md-5 order-md-3 d-md-block d-none">
                <img src="img/img-register01.png" class="d-block mx-auto img-fluid" alt="register">
            </div>
            <div class="col-md-2 col-3 order-md-2 align-self-start step">
                <img src="img/img-text-step01.png" class="d-block mx-auto img-fluid" alt="step 1">
            </div>
            <div class="col-md-5 col-9 order-md-1">
                <img src="img/img-register01.png" class="d-md-none d-block img-fluid mb-3" alt="register">
                <h3 class="mb-4"><span>REGISTER</span> TO OUR WEBSITE</h3>
                <p class="mb-5">Fill out the form and process desired payment.</p>
                <hr class="line1">
            </div>
        </div>
        <div class="row align-items-center pb-5 mb-5">
            <div class="col-md-5 order-md-1 d-md-block d-none">
                <img src="img/img-check-email01.png" class="d-block mx-auto img-fluid" alt="check email">
            </div>
            <div class="col-md-2 col-3 order-md-2 align-self-start step">
                <img src="img/img-text-step02.png" class="d-block mx-auto img-fluid" alt="step 2">
            </div>
            <div class="col-md-5 col-9 order-md-2">
                <img src="img/img-check-email01.png" class="d-md-none d-block img-fluid mb-3" alt="check email">
                <h3 class="mb-4"><span>CHECK</span> YOUR EMAIL</h3>
                <p class="mb-5">Check your email to confirm registration, payment and the activation code.</p>
                <hr class="line2">
            </div>
        </div>
        <div class="row align-items-center pb-5 mb-5">
            <div class="col-md-5 order-md-3 d-md-block d-none">
                <div class="download-app d-block mx-auto">
                    <a href="https://apps.apple.com/ph/app/era-sos/id1510601578" target="_blank"><img src="img/img-download-app-store01.png" class="d-block mx-auto my-2 img-fluid" alt="app store"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.mcdi.homedefence" target="_blank"><img src="img/img-download-google-play01.png" class="d-block mx-auto my-2 img-fluid" alt="google play"></a>
                </div>
            </div>
            <div class="col-md-2 col-3 order-md-2 align-self-start step">
                <img src="img/img-text-step03.png" class="d-block mx-auto img-fluid" alt="step 3">
            </div>
            <div class="col-md-5 col-9 order-md-1">
                <div class="d-md-none d-block download-app mb-3">
                    <a href="https://apps.apple.com/ph/app/era-sos/id1510601578" target="_blank"><img src="img/img-download-app-store01.png" class="d-block mx-auto my-2 img-fluid" alt="app store"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.mcdi.homedefence" target="_blank"><img src="img/img-download-google-play01.png" class="d-block mx-auto my-2 img-fluid" alt="google play"></a>
                </div>
                <h3 class="mb-4"><span>DOWNLOAD</span> APP</h3>
                <p class="mb-5">Download and install ERA SOS APP from Google Play Store or Apple Store.</p>
                <hr class="line1">
            </div>
        </div>
        <div class="row align-items-center pb-5 mb-5">
            <div class="col-md-5 order-md-1 d-md-block d-none">
                <img src="img/img-input-code01.png" class="d-block mx-auto img-fluid" alt="input code">
            </div>
            <div class="col-md-2 col-3 order-md-2 align-self-start step">
                <img src="img/img-text-step04.png" class="d-block mx-auto img-fluid" alt="step 4">
            </div>
            <div class="col-md-5 col-9 order-md-3">
                <img src="img/img-input-code01.png" class="d-md-none d-block img-fluid mb-3" alt="input code">
                <h3 class="mb-4"><span>INPUT</span> CODE</h3>
                <p class="mb-5">Enter the CODE.</p>
                <hr class="line2">
            </div>
        </div>
        <div class="row align-items-center pb-5 step-5-bg">
            <div class="col-md-5 order-md-3 d-md-block d-none">
                <img src="img/img-sos02.png" class="d-block mx-auto img-fluid" alt="sos">
            </div>
            <div class="col-md-2 col-3 order-md-2 align-self-start step">
                <img src="img/img-text-step05.png" class="d-block mx-auto img-fluid" alt="step 5">
            </div>
            <div class="col-md-5 col-9 order-md-1">
                <img src="img/img-sos02.png" class="d-md-none d-block img-fluid mb-3" alt="sos">
                <h3 class="mb-4"><span>ACCESS</span> APPROVED</h3>
                <p>Tap the radar icon to activate the APP.</p>
                <p class="mb-5">Note: A representative from our command center will call you within a minute to confirm your test.</p>
                <hr class="line1">
            </div>
        </div>
    </div>
    <div class="pb-5"></div>
</div>

<a id="partners" name="partners"></a>
<div class="partners text-white">
    <div class="container py-md-5 pb-4">
        <h2 class="pt-5 mb-5 text-white"><span>OUR</span> PARTNERS</h2>
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <img src="img/img-logo-hdt01.png" class="d-block mx-auto img-fluid mb-md-5 mb-2" alt="HDT">
            </div>
            <div class="col-lg-6 col-md-8">
                <img src="img/img-logo-vinculum01.png" class="d-block mx-auto img-fluid mb-5" alt="JR Towing">
            </div>
        </div>
        <hr class="w-25 mb-md-5">
    </div>
</div>

@endsection
