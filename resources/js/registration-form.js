import Gauge from '../../node_modules/vue-loading-spinner/src/components/Gauge';
import Circle2 from '../../node_modules/vue-loading-spinner/src/components/Circle2'
import MaskedInput from '../../node_modules/vue-masked-input/src/MaskedInput';
import vue2Dropzone from '../../node_modules/vue2-dropzone/dist/vue2Dropzone';
import '../../node_modules/vue2-dropzone/dist/vue2Dropzone.min.css';

const App = new Vue({

    el:"#app",

    components: {
        Circle2,
        Gauge,
        MaskedInput,
        vueDropzone: vue2Dropzone
    },

    data: {
        NumRegex: RegExp('^[0-9+()-]+$'),
        EmailRegex: RegExp('^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'),
        BirthdateRegex: RegExp('^[0-3][0-9][\/][0-3][0-9][\/][1-2][0-9][0-9][0-9]$'),
        bAgreedTAC: false,
        iPage: 1,
        iTotalPages: 3,
        sFileUploaderText: null,
        paynamicsHashCode:null,
        oForm: {
            sPromoCode: null,
            1:{
                sEmail: null,
                sFirstName: null,
                sLastName: null,
                sBirthdate: null,
                sHomeAddress: null,
                sMobileNum: null,
                sCodeWord: null,
                sMobileType: null,
                // sSpecificCond: null,
                //oImage: null,
                aEmergContacts: [
                    {
                        sEFullName: null,
                        sEMobileNum: null
                    }
                ]
            },
            2: {
                sMedicalCond: null,
                sHealthProvider: null,
                sPolicyNum: null,
                sHealthPlan: null
            },
            3: {
                sCarColor: null,
                sCarMake: null,
                sYearModel: null,
                sPlateNum: null
            },
            sSpecificCond: null,
            aFormErrors: []
        },
        annualMembership:1200, // Todo store this into database
        vat: .12, // Todo store this into database
        promoCode:null,
        promoDiscount:null,
        showSuccessMessage: false,
        showBillingDetails: false,
        showPaynamicsButton: false,
        loadPaymentForm: false,
        loadPayment: false,
        promocodeError:null,
        promocodeSuccess:null,
        isPaidByCompany: false, // skips registration flags by 100% discount
        company: null,
        submitted:false,
        isValidEmail: false
    },

    computed: {
        hasEmptyField: function() {
            return Object.values(this.oForm[this.iPage]).every(property => property) === false || this.oForm.aFormErrors.length != 0;
        },

        hasImage: function(){
            return $('#filePhoto').hasClass('dz-max-files-reached') == true
        },

        dictDefaultMessage: function(){
            return !this.sFileUploaderText ? '<i class="fas fa-upload"></i> Photo upload from phone *' : 'You uploaded ' + this.sFileUploaderText;
        },
        promoPrice:function(){
          return  (this.promoDiscount / 100 ) * (1200 - 144 );
          //return this.currency().format(price);
        },
        vatPrice:function(){
            return this.vat  * this.annualMembership;
        },
        membershipPrice:function(){
            return  this.annualMembership - (this.annualMembership * this.vat);
            //return this.currency().format(price);
        },
        totalPrice:function(){
           return this.annualMembership - this.promoPrice;
            //return this.currency().format(price);
        },
        dropzoneOptions: function(){
            return {
                url: 'https://httpbin.org/post', //url if dropzone will upload image
                thumbnailWidth: 120,
                thumbnailHeight: 120,
                maxFiles: 1,
                maxFilesize: 0.5,
                acceptedFiles: ".jpg, .jpeg, .png",
                addRemoveLinks: true,
                dictDefaultMessage: this.dictDefaultMessage,
                autoProcessQueue: false
            }
        },

        oBillingDetails: function(){
            return {
            sFirstName: this.oForm[1].sFirstName,
            sLastName: this.oForm[1].sLastName,
            sCompany: null,
            sStreetAddress: null,
            sProvince: null,
            sTownCity: null,
            sZipCode: null,
            sMobileNum: this.oForm[1].sMobileNum,
            stotalAmount: this.totalPrice
            }
        }
    },
    watch: {
        oForm : function(val){
            console.log('form has been change' + val);
        }
    },
    methods: {
        byPassPayment:function(data){
            axios.post('/payments/via-company',data) // bypass payment, company will process it
                .then(function (response) {
                    if(response.data.success == true){
                        window.location = '/payments/via-company-result?email=' + response.data.email +  '&discount_code='+ response.data.discount_code + '&success=true';
                    }else{
                        window.location = '/payments/via-company-result?success=false';
                    }
                })
                .catch(function (error) {
                    window.location = '/payments/via-company-result?success=false';
                    //console.log(error);
                });
        },
        currency:function(number){
            const formatter = new Intl.NumberFormat('en-PH', {
                style: 'currency',
                currency: 'PHP',
                minimumFractionDigits: 2
            });
            return formatter.format(number);
        },
        verifyCode: function(code){
            var me = this;
            axios.get('/admin/promocodes/verify/'+code)
                .then(function (response) {

                    if(response.data.error) {
                        me.promocodeError = response.data.error;
                        me.promocodeSuccess = null;
                        me.promoDiscount  = 0;
                    }else{
                        if(parseInt(response.data.percent) == 100){
                            me.isPaidByCompany = true;
                            me.promoDiscount = response.data.percent;
                            me.company = response.data.company;
                        }else {
                            me.promoDiscount = response.data.percent;
                            me.promocodeSuccess = 'Congratulations you receive ' + response.data.percent + '% discount';
                            me.promocodeError = null;
                        }
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        addContact: function(){
            event.preventDefault();

            this.oForm[1].aEmergContacts.push({
                sEFullName: null,
                sEMobileNum: null
            });
            
            this.setInputField();
        },

        removeContact: function(){
            event.preventDefault();
            var oElement = $(event.target).closest('div.emergencyContact');
            var aElements = $('#emergencyContacts').children('.emergencyContact');
            var iIndex = aElements.index(oElement);
            this.oForm[1].aEmergContacts.splice(iIndex, 1);
        },

        setInputField: function(){
            setTimeout(function(){
                let oTemplate = $('#emergencyContacts').children().last('.emergencyContact');
                oTemplate.find('.btnRemoveContact').prop('hidden', false);
                
                var sFieldId = new Date();
                sFieldId = sFieldId.getMilliseconds();

                oTemplate.attr('id', sFieldId);
            }, 0);   
        },

        dropzoneError: function(file, errorMsg){
            this.$refs.filePhoto.dropzone.removeFile(file);
            this.showFileError(errorMsg);
        },

        imageCropper: function (file){
    
            var me = this;
            var myDropZone = this.$refs.filePhoto.dropzone;
            $('#cropperModal').show();

            $('#btnCropImage').on('click', function() {
            
                var canvas = cropper.getCroppedCanvas({
                    width: 256,
                    height: 256
                });
            
                canvas.toBlob(function(blob) {
                    
                    myDropZone.createThumbnail(
                    blob,
                    myDropZone.options.thumbnailWidth,
                    myDropZone.options.thumbnailHeight,
                    myDropZone.options.thumbnailMethod,
                    false, 
                    function(dataURL) {
                        
                        myDropZone.emit('thumbnail', file, dataURL);
            
                        me.oForm[1].oImage = blob;
                    });
                });
            
                $('#cropperModal').hide();
            });

            var image = new Image();
            image.src = URL.createObjectURL(file);
            $('#cropperModalBody').children().remove();
            $('#cropperModalBody').append(image);
            
            var cropper = new Cropper(image, { aspectRatio: 1 });

            $('.btnCancel').on('click', function(){
                $('#cropperModalBody').children().remove();
                $('#cropperModal').hide();
                myDropZone.removeFile(file);
            })
        },

        completeBillingDetails: function(oBillingDetails){
            return Object.values(oBillingDetails).every(property => property) == true && this.oForm.aFormErrors.length === 0;
        },
        
        addedFile: function(upload) {
            this.hideFileError();
            this.sFileUploaderText = upload.name;
            this.imageCropper(upload);
        },


        nextPage: function() {
            if (this.iPage === 1) {
                this.checkPersonalInformation();
            } else if (this.hasEmptyField) {
                this.showAllErrors();
                return false;
            } else {
                this.iPage++;
                if(this.iPage > 1) {
                    window.scrollTo(150, 0)
                }
            }
        },

        checkPersonalInformation: function(){
            // var me = this;
            
            // this.checkEmail().then(function (response) {
            //     if (me.hasEmptyField) {
            //         me.showError('Email is required.', $('#txtEmail'));
            //         me.showAllErrors();
            //         me.showPersonalInfoError();
            //     } else if (response.data == false) {
            //         me.showError('Email already in use.', $('#txtEmail'));
            //     }
            //     // else {
            //     //     me.iPage++;
            //     // }
            // });
        },

        checkEmail : function (){
            var me = this;
            return axios.post('api/registration/email', {email: this.oForm[1].sEmail})
            .then(function (response){
                if(response.data == 1){
                    me.showError('Email already in use.', $('#txtEmail'));
                    me.isValidEmail = false;
                }else{
                    me.isValidEmail = true;
                }
            });
        },


        showAllErrors: function(){
            let oEmptyFields = $('#register-form input, #register-form select').filter(function() {
                return !this.value && !['txtPromoCode','txtSpecificCond', 'termsandconditions'].includes($(this).attr('id'));
            });
            App.showError('This field is required.', oEmptyFields);
        },

        showPersonalInfoError: function(){
            // if (this.hasImage == false) {
            //     App.showFileError('Please upload an image.');
            // }

            if (this.oForm[1].sMobileType == null) {
                App.showError('This field is required.', $('#txtMobileType'));
            }

            if (this.BirthdateRegex.test(this.oForm[1].sBirthdate) == false) {
                App.showError('Please provide valid birthdate.', $('#txtBirthdate'))
                return false;
            } 
        },

        previousPage: function(){
            this.hideError($('#app input'));
            this.iPage--;
        },

        validateBirthdate: function(event){
            if (event == false) {
                return false;
            } else if (this.oForm[1].sBirthdate == '__/__/____'){
                this.showError('This field is required.', $('#txtBirthdate'))
                return false;
            }
            
            this.BirthdateRegex.test(this.oForm[1].sBirthdate) == false ? this.showError('Please provide valid birthdate.', $('#txtBirthdate')) : this.hideError($('#txtBirthdate'));
        },

        validateEmail: function(){
            if (this.validateRequired() === true) {
                this.EmailRegex.test($(event.target)[0].value) == false ? this.showError('Please provide a valid email.') : this.hideError();
            }
        },

        validateNumber: function(){
            if (this.validateRequired() === true || $(event.target)[0].value != "") {
                this.NumRegex.test($(event.target)[0].value) == false ? this.showError('Must be a number.') : this.hideError();
            }
        },

        validateRequired: function(){
            if ($(event.target)[0].value == false) {
                this.showError('This field is required.');
                return false;
            } else {
                this.hideError();
                return true;
            }
        },

        showError: function(sErrorMsg, oElement = $(event.target)){
            this.saveError(oElement)
            oElement.addClass('is-invalid');
            oElement.next('.invalid-feedback').remove();
            oElement.after("<div class=\"invalid-feedback\">" + sErrorMsg + "</div>");
        },

        hideError: function(oElement = $(event.target)){
            this.removeError(oElement);
            oElement.removeClass('is-invalid');
            oElement.next('.invalid-feedback').remove();
        },

        showFileError: function(sErrorMsg){
            if (sErrorMsg != 'You can not upload any more files.'){
                this.saveError($('#filePhoto'));
            }

            $('#filePhoto').addClass('fileUploader-error');
            $('#filePhoto').next().remove();
            $('#filePhoto').after("<div class=\"invalid-feedback\" style=\"display:block;\">" + sErrorMsg + "</div>");
        },

        hideFileError: function(){
            this.removeError($('#filePhoto'));
            $('#filePhoto').next('.invalid-feedback').remove();
            $('#filePhoto').removeClass('fileUploader-error');
        },

        saveError: function(oElement){
            if (!this.oForm.aFormErrors.includes(oElement.attr("id")) && oElement.attr("id")) {
                this.oForm.aFormErrors.push(oElement.attr("id"));
            }
        },

        removeError: function(oElement){
            if (this.oForm.aFormErrors.includes(oElement.attr("id"))) {
                let iIndex = this.oForm.aFormErrors.indexOf(oElement.attr("id"));
                this.oForm.aFormErrors.splice(iIndex, 1);
            }
        },
        submit(){
            
            if (this.hasEmptyField){
                this.showAllErrors();
                return false;
            } else if (this.bAgreedTAC === false){
                return false;
            }

            if(this.isValidEmail==false){
                this.checkEmail();
                return false;
            }

            
            this.submitted=true;
            this.iPage = null;
            this.loadPaymentForm = true;
            var me = this;

            let payload = new FormData();
            payload.append("txtEmail", this.oForm[1].sEmail);
            //payload.append("txtPromoCode", this.oForm.sPromoCode);
            payload.append("txtFirstName", this.oForm[1].sFirstName);
            payload.append("txtLastName", this.oForm[1].sLastName);
            payload.append("txtBirthdate",  this.oForm[1].sBirthdate);
            payload.append("txtHomeAddress", this.oForm[1].sHomeAddress);
            payload.append("txtMobileNum", this.oForm[1].sMobileNum);
            payload.append("aEmergencyContacts", JSON.stringify(this.oForm[1].aEmergContacts));
            payload.append("txtMobileType", this.oForm[1].sMobileType);
            payload.append("txtSpecificCond", this.sSpecificCond);
            payload.append("txtCodeWord", this.oForm[1].sCodeWord);
            axios.post('/api/registration/store', payload, {
                    headers: { 'Content-Type': 'multipart/form-data'}
                }).then(function (response) {
                    console.log(response);
                    me.loadPaymentForm = false;
                    me.showSuccessMessage = true;
                    me.showBillingDetails = true;
                })
                .catch(function (error) {
                    console.log(error);
                });

        },
        submitBilling: function(){

            let me = this;
            let oBillingDetails = this.oBillingDetails;
            delete oBillingDetails.sCompany;

           

            if(this.completeBillingDetails(oBillingDetails) === false){
                let oEmptyFields = $("#billingForm input, #txtBillingProvince").filter(function() {
                    return !this.value && ['txtBillingCompany', 'txtBillingMiddleName'].includes($(this).attr('id')) === false;
                });

                App.showError('This field is required.', oEmptyFields);
    
                return false;
            }


            let payload = {
                sFirstName: this.oBillingDetails.sFirstName,
                sLastName: this.oBillingDetails.sLastName,
                sCompany: this.oBillingDetails.sCompany,
                sStreetAddress: this.oBillingDetails.sStreetAddress,
                sAddress: this.oForm[1].sHomeAddress,
                sProvince: this.oBillingDetails.sProvince,
                sTownCity: this.oBillingDetails.sTownCity,
                sZipCode: this.oBillingDetails.sZipCode,
                sMobileNum: this.oBillingDetails.sMobileNum,
                sEmail:this.oForm[1].sEmail,
                sTotalAmount: this.totalPrice,
                sPromoCode: this.promoCode
            };

            //console.log(payload)
            axios.post('/payments', payload)
            .then(function (response) {
                me.paynamicsHashCode  = response.data.paymentrequest;
                me.showPaynamicsButton = true;
                me.showBillingDetails  = false;
            })
            .catch(function (error) {
                console.log(error);
            });
        },

        proceedPayment: function(){
            App.loadPayment = true;
            $('#paymentConfirmation').submit();
        }
    }
});