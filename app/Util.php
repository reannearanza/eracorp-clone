<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Util extends Model
{
    public static function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public static function decodeBase64($base64encoded){
        $encoded = urldecode($base64encoded);
        $encoded = str_replace(" ",'+',$encoded);
        return base64_decode($encoded);
    }

    public static function isAdmin(){
        if(!\Gate::allows('isAdmin')){
            abort(403,"Invalid user permission!");
        }
    }

    


}
