@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Payments</div>
                                <div class="card-body">
                                    <table class="table">
                                        <tr>
                                            <td>ID</td>
                                            <td>User ID</td>
                                            <td>Email</td>
                                            <td>Amount Paid</td>
                                            <td>Discount Code</td>
                                            <td>Company</td>
                                            <td>Discount %</td>
                                            <td>Payment Method</td>
                                            <td>Status</td>
                                            <td>Created At</td>
                                            <td>Action</td>
                                        </tr>
                                        @foreach($records as $record)
                                            <tr>
                                                <td>{{$record->id}}</td>
                                                <td>{{$record->user_id}}</td>
                                                <td>{{$record->email}}</td>
                                                <td>{{$record->amount_paid}}</td>
                                                <td>{{$record->discount_code}}</td>
                                                <td>{{$record->company}}</td>
                                                <td>{{$record->discount_percent}}</td>
                                                <td>{{$record->method}}</td>
                                                <td>{{ucfirst($record->status)}}</td>
                                                <td>{{ date('Y-m-d',strtotime($record->created_at))}}</td>
                                                <td>
                                                    <a class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                                       href="#" role="button"
                                                       aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a
                                                                class="dropdown-item"
                                                                @if( strtolower($record->method) == 'via-company')
                                                                href="/admin/payments/edit/{{$record->id}}"
                                                                @else
                                                                onclick="alert('You may only edit payments via-company')"
                                                                @endif
                                                        >
                                                            Edit
                                                        </a>
                                                        {{--<a class="dropdown-item"--}}
                                                           {{--href="/admin/payments/delete/{{$record->id}}">--}}
                                                            {{--Delete--}}
                                                        {{--</a>--}}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                {{$records->links()}}
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- /.row-->
                </div>
            </div>
        </main>

    </div>
@endsection