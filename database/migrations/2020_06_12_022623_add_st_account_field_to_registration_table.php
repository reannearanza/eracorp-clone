<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStAccountFieldToRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('registrations', function(Blueprint $table) {
            if (!Schema::hasColumn('registrations', 'st_account')) {
                Schema::table('registrations', function (Blueprint $table) {
                    $table->string('st_account', 60)->after('id')->nullable(); // see EC-94
                });
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registration', function (Blueprint $table) {
            //
        });
    }
}
