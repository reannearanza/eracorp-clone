@extends('layouts.admin')
@section('content')
<div class="container">
    <h1 class="py-4">Batch Registration</h1>
    <form action="import-store" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="fileUpload">Upload Excel File</label>
            <input type="file" class="form-control-file" name="fileUpload">
        </div>
        <button type="submit" class="btn btn-danger">Upload</button>
        @csrf
    </form>
</div
@endsection