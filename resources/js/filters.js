import Vue from 'vue'

Vue.filter('ucwords',function(str){
    return str.toLowerCase().replace(/(?<= )[^\s]|^./g, a=>a.toUpperCase());
})

// Vue.filter('trim',function(str){
//     return str.trim();
// })