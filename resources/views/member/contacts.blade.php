@extends('layouts.admin')
@section('content')
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Emergency Contacts</h3>
                            <emergency-contacts :user-id="{{$userId}}" :contacts="{{ json_encode($contacts)}}"></emergency-contacts>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
                                    