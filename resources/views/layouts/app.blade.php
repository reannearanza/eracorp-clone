<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ERA - Help within reach</title>

        <!-- STYLES -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/eracorp.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="fontawesome/css/all.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/img-favicon01.png') }}"/>
        <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
        <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
        <!-- FONTS -->
        <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/img-favicon01.png') }}"/>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
        
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168541715-1"></script>
    <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-168541715-1'); </script>
        
    <div class="navigation text-white sticky-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark px-0">
                <a class="navbar-brand" href="/">
                    <img src="{{ asset('img/img-logo01.png') }}" alt="ERACorp" class="logo">
                </a>
                <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse pt-lg-0 pt-5" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">HOME</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#chairmanmessage">CHAIRMAN MESSAGE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#about-us">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#how-it-works">HOW IT WORKS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#partners">OUR PARTNERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">CONTACT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/registration">REGISTER</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">LOGIN</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
        
    @yield('content')
    @yield('script')
    
    <a id="contact" name="contact"></a>
    <div class="footer text-white">
        <div class="container py-md-5 pb-4">
            <h2 class="pt-5 text-white"><span>CONTACT</span> US</h2>
            <p class="mb-5">Need help? We can give immediate answer to your inquiries. <br class="d-xl-block d-none">
                Reach out to our customer support through the following channels from 8am to 5pm, Mondays through Sundays.</p>
            <div class="row">
                <div  id="vue-form" class="col-md-6">
                    <form v-if="!formSuccess" @submit.prevent="submit">
                        <div class="form-group">
                            <input type="text" class="form-control rounded-0" id="name"  v-model="name" placeholder="NAME">
                            <div v-if="nameError" style="color:red;font-weight: bolder;margin-top:2px">Name cannot be empty</div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control rounded-0" id="email" v-model="email" placeholder="EMAIL">
                            <div v-if="emailError" style="color:red;font-weight: bolder;margin-top:2px">Email cannot be empty</div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control rounded-0" id="message" rows="5" v-model="message" placeholder="MESSAGE"></textarea>
                            <div v-if="messageError" style="color:red;font-weight: bolder;margin-top:2px">Message cannot be empty</div>
                        </div>
                        <div class="form-group text-center pt-4 pb-md-0 pb-5">
                            <button type="submit" class="btn btn-send" :disabled="isSending" >
                                <i v-bind:class="{ 'fas fa-cog fa-spin mr-2':isSending }"></i>SEND
                            </button>
                        </div>
                    </form>
                    <div v-if="formSuccess" class="alert alert-success" role="alert">
                        <p class="font-weight-bolder">Thank you for contacting us, we will get back to you shortly.</p>
                    </div>

                </div>
                <script>
                    var vueForm = new Vue({
                        el: '#vue-form',
                        data: {
                            name: '',
                            email: '',
                            message: '',
                            formSuccess: false,
                            isSending: false,
                            nameError: false,
                            emailError: false,
                            messageError: false,
                        },
                        methods :{
                            submit: function(){
                                var me = this;
                                me.formSuccess = false;
                                var errors = [];
                                if(!me.name){
                                    errors.push({name: 'Name cannot be empty'});
                                    me.nameError = true;
                                }
                                if(!me.email){
                                    errors.push({email: 'Name cannot be empty'});
                                    me.emailError = true;
                                }
                                if(!me.message){
                                    errors.push({message: 'Message cannot be empty'});
                                    me.messageError = true;
                                }

                                if(errors.length){
                                    return false;
                                }
                                me.isSending = true;
                                axios.post('/api/contact-us/store', {
                                    name: me.name,
                                    email: me.email,
                                    message: me.message
                                })
                                    .then(function (response) {
                                        me.formSuccess = true;
                                        me.isSending=false;
                                        me.name  = "";
                                        me.email = "";
                                        me.message = "";
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                            }
                        }
                    })
                </script>
                <div class="col-md-6">
                    <div class="media">
                        <i class="fas fa-lg fa-map-marker-alt pt-2 mr-3"></i>
                        <div class="media-body">
                            <p>Suite 206 Page one Bldg. 1215 Acacia Ave., Madrigal Business Park, Ayala, Alabang, Muntinlupa City 1780 Metro Manila, Philippines</p>
                        </div>
                    </div>
                    <?php /*?>
                    <div class="media">
                        <i class="fas fa-phone-alt pt-1 mr-3"></i>
                        <div class="media-body">
                            <p>Globe: +639 060 953 635<br>
                                Smart: +639 198 885 475</p>
                        </div>
                    </div>
                    <?php */?>
                    <div class="media">
                        <i class="fas fa-envelope pt-1 mr-3"></i>
                        <div class="media-body">
                            <p><a class="text-white" href="mailto:era.csr@hdt.ph" target="_blank">era.csr@hdt.ph</a></p>
                        </div>
                    </div>
                    <div class="text-center mt-md-5 pt-5">
                        <a href="https://www.facebook.com/erasosph/" class="text-white px-2" target="_blank"><i class="fab fa-facebook-f fa-2x"></i></a>
                        <a href="https://twitter.com/erasosph" class="text-white px-2" target="_blank"><i class="fab fa-twitter fa-2x"></i></a>
                        <a href="https://instagram.com/erasosph?igshid=1v61tuny5sj48" class="text-white px-2"><i class="fab fa-instagram fa-2x"></i></a>
                    </div>
                </div>
            </div>
            <img src="img/img-lifeline02.png" class="d-block mx-auto img-fluid my-5" alt="">
            <p class="text-center"><small>All rights reserved 2020 Emergency Rapid Assistance Corporation. Powered by <a href="https://rga-itsolutions.com/" target="_blank" class="text-white">RGA-ITSolutions.com</a><br>
                <a href="/privacy.html" class="text-white">Privacy Policy</a></p>
        </div>
    </div>
    {{ TawkTo::widgetCode() }}
        <!-- SCRIPTS -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/smoothscroll.js"></script>
    </body>
</html>
