@extends('layouts.app')
@section('content')

<div class="privacy">
    <div class="container py-5">
        <div class="card mb-4">
            <div class="card-body">
                <p><strong>Home Defense Technology </strong><br>
                    Unit 502B Catleya  Condominium<br>
                    Salcedo St., Legaspi  Village<br>
                Makati, Metro Manila</p>
                <hr>
                <h3><strong>Terms And Agreement</strong></h3>
                <p>This agreement (the "Agreement") is made as of the  Effective Date indicated below by and between the company set forth above (the  "Company") and the owner of the home or business (the "Customer") located at  the address shown below (the "Premises"). The Company agrees to provide, or  cause to be provided, the alarm monitoring services for the alarm system (the  "System") installed at the Premises.</p>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Subscriber's Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Contact 2">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Effective Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Billing Address">
                                    <small class="form-text text-muted">(If different from premises)</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Phone">
                                    <small class="form-text text-muted">(Spouse/Resident)</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="" placeholder="Email address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Signed">
                                    <small class="form-text text-muted">By signing above, Customer acknowledges receiving appropriate terms and conditions for the following provider</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Service Rate">
                                    <small class="form-text text-muted">Extended Service Option</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Total Monthly Fee*">
                                    <small class="form-text text-muted">*PLUS OTHER CHARGES INCLUDING APPLICABLE TAX</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Monthly Payment Enrollment">
                                    <small class="form-text text-muted">By signing above, Customer authorizes Company to collect the Total Monthly Fee   and   any   other   amounts   due hereunder.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p><strong>1. THE SERVICE</strong>: Upon receipt of a non-verified SOS  Panic Alarm, Company will attempt to contact the Subscriber and/or the persons  submitted to Company on Customer's contact list to verify whether an emergency  exists. Methods of verification used by Company include verbal verification <strong>(Verbal Codeword)</strong> or receipt of a  cancel or false signal verified by the client. In necessary situations, Company  will attempt to contact the appropriate responding agency, or a guard service.  Company and Customer must comply with local notification and response  requirements, which may now or in the future include verbal or visual  verification of an emergency condition prior to response. Customer agrees to  pay any charge associated with this requirement, including any fees that they  may incur from receiving emergency services such as: ambulatory, roadside  repair, towing, and public transport. Customer is aware that response time will  vary on the availability, road condition, and distance of the responder from  the location of the emergency site.<br>
                It is the Customer 's  responsibility to test the functionality of all components of his/her System  each month.  Customer agrees that remote  medical assistance will have to be consented by the person who will administer  first aid and the person receiving first aid.   Any procedure that requires professional medical intervention such as  CPR can only be administered if the person performing the procedure is  certified or licensed. <br>
                Customer agrees and fully understand that it's their sole  responsibility to ensure that the internet connection Via Wi-Fi and mobile  phone data connection is fully functional and that the SOS Panic application  maybe susceptible to latency, delayed notification and or service interruption  depending on the signal strength of their internet connection or cell phone  signal. Subscribers will be trained to understand, navigate, and know the  restrictions of the given application (ERA SOS) for their alarm system for  maximum effectivity. </p>
                <p><strong>2. DISCLAIMER OF  WARRANTIES: NEITHER COMPANY NOR ITS CONTRACTOR REPRESENTS OR WARRANTS THAT THE  SYSTEM OR THE MONITORING SERVICES WILL PREVENT ANY LOSS BY BURGLARY, FIRE,  ROBBERY OR OTHERWISE, OR WILL, IN ALL CASES, PROVIDE THE SPECIFIED NOTIFICATION  SERVICE. </strong>Customer  understands that there are no warranties which extend beyond the face of this  Agreement and acknowledges that neither Company nor its contractor has made any  representation or warranty, express or implied, including without limitation,  about the condition of the System or monitoring service, their merchantability,  or their fitness for any particular purpose, other than those expressly  contained in this Agreement. Customer understands and acknowledges that the  System, Transmission System (See Section 9), or Company's or its contractor's  equipment may not function properly; that the Company or its contractor may not  respond properly to the receipt of an alarm signal; and that neither Company  nor its contractor has control over the response time or capability of any  agency or person notified. CUSTOMER ALSO UNDERSTANDS THAT IN THE EVENT THAT THE  COMPANY IS DETERMINED TO BE DIRECTLY OR INDIRECTLY LIABLE FOR ANY LOSS, DAMAGE,  OR INJURY THAT THE LIMIT OF LIABILITY IN SECTION 5 APPLIES.</p>
                <p><strong>3. SERVICE FEES AND  TERM OF AGREEMENT: </strong>This  Agreement shall continue for an initial term of __12__ months ("Initial Term")  unless earlier terminated pursuant to the provisions hereof, and shall  thereafter automatically renew for successive twelve (12) months term(s)  ("Renewal Term") unless cancelled by either party at least thirty (30) days  before the end of the then-current term. Customer may cancel this Agreement by  calling Company at least thirty (30) days before the end of the then-current  term. If cancelled, this Agreement ends on the last day of the then-current  term. Customer agrees to pay the Total Monthly Fee above plus all applicable  taxes, permit fees, false alarm charges, communication charges, failed payment  charges, emergency service related charges, service charges, late charges,  surcharges, or other related charges (collectively, "Other Charges"), if  applicable, whether imposed on Company or Customer. Company may increase the  Total Monthly Fee at any time after the first twelve (12) months. If Customer  objects in writing to the increase of the Total Monthly Fee within thirty (30)  days of receiving notice of the increase on Customer's invoice (including  invoices available through Company's website), and if Company does not waive  the increase, then Customer may terminate this Agreement effective thirty (30)  days after Company's receipt of Customer's written notice of termination, and  Customer will not have to pay the contract termination charges described in  Section 8.</p>
                <p><strong>4. Payment/Late  Charges: </strong>By signing  this contract, Customer understands and agrees to receive and pay monthly bill  from the company one month after the day of_ Month _____, Year____ for duration  of _______ months.  Company may impose a  late charge on each payment that is past due and a fee for any check or other  instrument (including credit card charge backs) returned for any reason. In the  event any late charges or other charges are held to be in excess of the highest  lawful amount, such charges shall be reduced to the highest lawful amount, and  any excess charges will be promptly refunded or credited to Customer's account. <br>
                    If a customer is not satisfied with the service, request for  a refund may be done by contacting the customer service department. Refund can  be requested within 7 days from subscription date. After 7 days, customer is no  longer eligible to request for a refund or will not get a refund. Reversal of  funds will take about 7-14 business days. </p>
                <p>FINANCIAL  DISCLOSURE STATEMENT<br>
                    THERE IS NO FINANCE CHARGE OR COST OF CREDIT (0% APR)  ASSOCIATED WITH THIS AGREEMENT</p>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Number of payments for the initial term is">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Amount of each payment is">
                                    <small class="form-text text-muted">(Total Monthly Fee from above)</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Total of Payments for the initial term is">
                                    <small class="form-text text-muted">(Includes all applicable taxes, fees and fines)</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p><strong>CUSTOMER RESPONSIBILITY  TO READ AGREEMENT:</strong> CUSTOMER HAS READ AND UNDERSTOOD ALL TERMS AND CONDITIONS INCLUDING THOSE CONTAINED  ON THE NEXT PAGE AND INCORPORATED BY REFERENCE HEREIN. THESE TERMS AND  CONDITIONS INCLUDE A DISCLAIMER OF WARRANTIES IN SECTION 2, A LIST OF  CUSTOMER'S DUTIES IN SECTION 6, THE CONTRACT TERMINATION CHARGE IN SECTION 8,  AND CUSTOMER'S CONSENT TO BE CONTACTED IN SECTION 11.  CUSTOMER AUTHORIZES PAYMENT OF ALL AMOUNT DUE  TO COMPANY BY THE METHOD SPECIFIED ABOVE.   CUSTOMER ALSO ACKNOWLEDGES BEING ORALLY INFORMED OF CUSTOMER'S RIGHT TO  CANCEL AT THE TIME OF EXECUTION OF THIS AGREEMENT.  <br>
                    <br>
                <strong>5. COMPANY IS NOT AN INSURER AND  LIMITATION OF LIABILITY: CUSTOMER ACKNOWLEDGES AND AGREES THAT COMPANY IS NOT  AN INSURER; THAT CUSTOMER ASSUMES ALL RISK OF PERSONAL INJURY AND LOSS OR  DAMAGE TO CUSTOMER'S PREMISES OR TO THE CONTENTS THEREOF: </strong>Customer further  acknowledges and agrees that if any insurance is desired, customer must obtain  it. In addition to the Company's other rights at law under this agreement, the  customer specifically releases the Company from any liability for any event or  condition covered by the Customer's insurance. <strong>CUSTOMER UNDERSTANDS AND AGREES THAT IF COMPANY SHOULD BE FOUND LIABLE  OF LOSS OR DAMAGE DUE TO COMPANY'S NEGLIGENCE, FAILURE TO PERFORM ANY OF THE  OBLIGATIONS HEREIN, VIOLATION OF ANY APPLICABLE LAW (INCLUDING CONSUMER  PROTECTION LAWS), OR FAILURE OF THE MONITORING SERVICE OR THE EQUIPMENT N ANY  RESPECT WHATSOEVER, COMPANY'S LIABILITY SHALL BE LIMITED TO THE SUM OF TEN  THOUSAND PESOS (10,000.00) AND THIS LIABILITY SHALL BE COMPANY'S SOLE AND  EXCLUSIVE LIABILITY.</strong></p>
                <p><strong>6. FAMILIARIZATION  PERIOD: CUSTOMER UNDERSTANDS THAT THE COMPANY WILL ALLOT A FAMILIARIZATION  PERIOD OF 7 DAYS AFTER THE DATE OF INSTALLATION. DURING FAMILIARIZATION PERIOD,  CUSTOMER UNDERSTANDS THAT, DURING SUCH PERIOD, COMPANY HAS NO OBLIGATION TO  NOTIFY ANY AUTHORITIES OF ANY INTRUSION ALARM SIGNAL THE COMPANY RECEIVES FROM  CUSTOMER'S PREMISES, EVEN IF DUE TO AN ACTUAL EMERGENCY EVENT.</strong></p>
                <p><strong>7. DEFAULT,  DISCONNECTION AND REMEDIES: </strong>Customer will be in default and breach of this agreement if  Customer 1) fails to pay any fees or charges when due; 2) generates, in  Company's sole judgment, excessive false alarms; or 3) fails to perform other  obligations set forth in this Agreement. In the event of a default, the Company  may, by notice to Customer, terminate Customer's monitoring services and DISABLE  ERA SOS APPLICATION. Customer will remain responsible for all charges incurred  prior to the effective date of the service termination Customer agrees that the  charges due under this Agreement are based on Customer's agreement to receive  and pay for the services during the Initial Term and any Renewal Term and that  Company has relied upon this agreement and has incurred costs in deciding to  enter into this contract. <strong>If Customer  breaches this Agreement during its Initial Term or any Renewal Term, Customer  will also pay an amount equal to eighty (80%) percent of the remaining payments  owed during the Initial or any Renewal Term and any related levies, court  costs, collection costs, and attorney fees. This termination charge is not a  penalty; it is a charge to compensate Company for Customer's failure to  maintain services for the initial Term or any Renewal Term.  All amounts are due immediately without  presentment, demand, protest or further notice, all of which Customer expressly  waives.</strong></p>
                <p><strong>8. FALSE ALARMS: </strong>Customer understands that local  authorities may impose fines for false alarms or signals, and Customer agrees  to be responsible for these fines and any related costs whether they are levied  directly on Customer or on the Company, its contractors, or subcontractors. Intentional  false alarm such as pranks will result to termination of subscription. </p>
                <p><strong>9</strong>. <strong>ENTIRE AGREEMENT: </strong>This Agreement is intended by the parties as a  final expression of their agreement and as a complete and exclusive statement  of the terms thereof. Company's duty and obligation to provide monitoring  service to Customer arise solely from this Agreement. This Agreement supersedes  all prior representations, understandings, or agreements of the parties. This  Agreement can only be modified (a) in writing, signed by the parties or their  duly authorized agents or (b) by bill message on Customer's invoice, email or  other notice from Company to Customer provided that Customer does not object in  writing within thirty (30) days after receiving the notice. No waiver or breach  of any term or condition of this Agreement shall be construed to be a waiver of  any succeeding breach. Customer agrees that Company may convert this Agreement  to electronic media, which may serve as the exclusive original. If a court  determines that any provision of this contract is invalid or unenforceable,  that provision shall be deemed amended and enforced to the maximum extent  permitted by law. Each and every other provision of this contract shall continue  to be valid and enforceable.</p>
                <strong>10.  PRIVACY</strong>:  Company will use commercially reasonable efforts to maintain the privacy of  Customer's information. Customer understands that Company cannot guarantee  privacy and agrees not to hold the Company liable for any claims, loss,  damages, or costs that may result from loss of privacy. Customer consents to  Company contacting him/her at the mailing address, email address and phone  number(s) Customer provides for any purpose related to this Agreement using any  method, including automated technology, prerecorded messages or text messages.  If Customer's wireless provider charges Customer for text, data usage, or email  messages, Customer is responsible for any such charges. Customer consents to  the recording of all communications between the Customer and Company. Customer  represents that he/she has obtained the above-referenced consents from any  third-parties, whose emails and phone number(s) Customer provides to Company  (including emergency contacts). Customer agrees to contact from Company and/or  its outside collection agencies in connection with all matters relating to  unpaid past due charges billed by Company. Customer agrees that contacts to  collect unpaid past due charges may be made to any mailing address, phone number  or any email address Customer provides, and such contact may be made using any  method, including automated technology, prerecorded messages or text messages.  Customer agrees and acknowledges that any e-mail address that the Customer  provides to Company is his/her private address and is not accessible to  unauthorized third parties.
            </div>
        </div>
    </div>
</div>

@endsection
